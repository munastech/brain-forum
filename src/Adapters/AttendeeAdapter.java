package Adapters;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.brainforum.org.AttendeeDesc;
        import com.brainforum.org.R;
        import com.nostra13.universalimageloader.core.DisplayImageOptions;
        import com.nostra13.universalimageloader.core.ImageLoader;
        import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
        import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

        import java.util.ArrayList;
        import java.util.List;
        import java.util.Locale;

        import DataStructure.AppGlobal;
        import DataStructure.AttendeeData;
        import DataStructure.ImageDownloader;
        import DataStructure.SpeakerData;

/**
 * Created by Muhammad Nasir on 1/26/2015.
 */
public class AttendeeAdapter extends BaseAdapter {
    Context mContext;
    int layoutResourceId;
    public List<AttendeeData> data;
    public List<AttendeeData> arraylist;
    viewHolder holder;
    DisplayImageOptions options;

    ImageDownloader mImageDownloader=new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    public AttendeeAdapter(Context mContext,int LayoutResourceID, List<AttendeeData> data)
    {
        this.mContext=mContext;
        this.layoutResourceId=LayoutResourceID;
        this.data=data;
        this.arraylist=new ArrayList<AttendeeData>();
        arraylist.addAll(data);
        holder=new viewHolder();
        if(!imageLoader.isInited()){

            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img1)
                .showImageForEmptyUri(R.drawable.img1)
                .showImageOnFail(R.drawable.img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        if(convertView==null){
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, viewGroup, false);
        }
        holder.jobTitle=(TextView)convertView.findViewById(R.id.job_title);
        holder.name=(TextView)convertView.findViewById(R.id.full_name);
        holder.imageView=(ImageView)convertView.findViewById(R.id.attendee_image);
        holder.viewProfile=(TextView)convertView.findViewById(R.id.view_profile);

        holder.jobTitle.setText(data.get(i).job_title);
        holder.name.setText(data.get(i).name);

        holder.viewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mContext,AttendeeDesc.class);
                intent.putExtra("position",i);
                mContext.startActivity(intent);
            }
        });
        final String imageName=data.get(i).image;
/*check
		 * check if image came from server becaues if image came from server it save in the database with extension like
		 * yuoga.jpg or yuoga.png but if it was image from drawable using like yuoga.png we remove extension of image name and save it
		 * in db like "yuoga" no ".png" because we are getting image from drawable with string name so its remove extendion .the method used to get id is define in utils class
		 * im.setImageResource(Utils.getId(imageName)); this line for image in drawable for static purpose.......
		 * */
        //code for update image
            Bitmap bmp=mImageDownloader.getImage(imageName);
            //check if image was in cache
            if(bmp!=null){
                Log.d("Image Available", "Yes");
                holder.imageView.setImageBitmap(bmp);

            }
            //else part if not downloaded then download it...........
            else{
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if(imageName!=null)mImageDownloader.downloadImage(imageName);
                    }
                };
                thread.start();
                //in case not download as soon as thread coplete its work set dummy image
                //mImg.setImageResource(R.drawable.gallery_test);
                String url= AppGlobal.ServerImagePath+imageName;

                //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                ////////////////////
                imageLoader.displayImage(url, holder.imageView, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }
                        }
                );
            }

        return convertView;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(arraylist);
        }
        else
        {
            for (AttendeeData ad : arraylist)
            {
                if (ad.name.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    data.add(ad);
                }
            }
        }
        notifyDataSetChanged();
    }
    public class viewHolder
    {
        ImageView imageView;
        TextView name,jobTitle,viewProfile;
    }
}

