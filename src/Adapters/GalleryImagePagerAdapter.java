package Adapters;

import com.brainforum.org.Gallery;
import com.brainforum.org.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import DataStructure.AppGlobal;
import DataStructure.GalleryData;
import DataStructure.ImageDownloader;
/**
 * Created by Muhammad Nasir on 2/10/2015.
 */



public class GalleryImagePagerAdapter extends PagerAdapter {
    Activity mContext;
    private LayoutInflater inflater;
    String imageName;
    DisplayImageOptions options;
    List<GalleryData> mgGallery = AppGlobal.galleryList;

    ImageDownloader mImageDownloader=new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    public GalleryImagePagerAdapter(Activity mcon,List<GalleryData> data ) {
        super();
        this.mContext=mcon;
        mgGallery=data;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(!imageLoader.isInited()){

            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        }
        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.img_1)
                .showImageOnFail(R.drawable.img_1)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

    }

    @Override
    public int getCount() {
        return mgGallery.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        //		return view == ((ImageView) object);
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;
        ImageButton btnClose;
        TextView title,date_time;
        final ProgressBar spinner;

        View imageLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);
        assert imageLayout != null;
        imgDisplay = (ImageView) imageLayout.findViewById(R.id.imgDisplay);
        spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
        btnClose = (ImageButton) imageLayout.findViewById(R.id.btnClose);
        title=(TextView)imageLayout.findViewById(R.id.image_title);
        date_time=(TextView)imageLayout.findViewById(R.id.date_time);


        imageName=mgGallery.get(position).image_name;
        title.setText(mgGallery.get(position).title);
        String date=null;
        try {
            date=new SimpleDateFormat("HH:mm.ddMMMyy").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(mgGallery.get(position).date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("Date/original",date+"/"+mgGallery.get(position).date);
        date_time.setText(date);






        // close button click event
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.finish();
              }
        });


		/*check
		 * check if image came from server becaues if image came from server it save in the database with extension like
		 * yuoga.jpg or yuoga.png but if it was image from drawable using like yuoga.png we remove extension of image name and save it
		 * in db like "yuoga" no ".png" because we are getting image from drawable with string name so its remove extendion .the method used to get id is define in utils class
		 * im.setImageResource(Utils.getId(imageName)); this line for image in drawable for static purpose.......
		 * */
        //code for update image
        if(imageName.contains(".png")||imageName.contains(".jpg") ||imageName.contains(".JPG")){
            Bitmap bmp=mImageDownloader.getImage(imageName);
            //check if image was in cache
            if(bmp!=null){
                imgDisplay.setImageBitmap(bmp);

            }
            //else part if not downloaded then download it...........
            else{
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if(imageName!=null)mImageDownloader.downloadImage(imageName);
                    }
                };
                thread.start();
                //in case not download as soon as thread coplete its work set dummy image
                //imgDisplay.setImageResource(R.drawable.gallery_test);


                imageName=imageName.replaceAll(" ", "%20");
                String url=AppGlobal.ServerImagePath+imageName;

                //UrlImageViewHelper.setUrlDrawable(imgDisplay, url, R.drawable.gallery_test);

/////

                imageLoader.displayImage(url, imgDisplay, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        spinner.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        String message = null;
                        switch (failReason.getType()) {
                            case IO_ERROR:
                                message = "Input/Output error";
                                break;
                            case DECODING_ERROR:
                                message = "Image can't be decoded";
                                break;
                            case NETWORK_DENIED:
                                message = "Downloads are denied";
                                break;
                            case OUT_OF_MEMORY:
                                message = "Out Of Memory error";
                                break;
                            case UNKNOWN:
                                message = "Unknown error";
                                break;
                        }
                        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();

                        spinner.setVisibility(View.GONE);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        spinner.setVisibility(View.GONE);
                    }
                });

/////

            }
        }
        //if it dosenot contain extension it means its was image from drawable use getid method define in util class for for geting id after giving image name.
        else{
            imgDisplay.setImageResource(AppGlobal.getResourceId(mContext,imageName,"drawable"));


        }
        //		((ViewPager) container).addView(imgDisplay, 0);
        ((ViewPager) container).addView(imageLayout);

        //	        return imgDisplay;
        return imageLayout;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        //		((ViewPager) container).removeView((ImageView) object);
        ((ViewPager) container).removeView((RelativeLayout) object);
    }

}