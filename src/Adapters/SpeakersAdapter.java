package Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainforum.org.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import DataStructure.AppGlobal;
import DataStructure.ImageDownloader;
import DataStructure.SpeakerData;

/**
 * Created by Muhammad Nasir on 1/26/2015.
 */
public class SpeakersAdapter extends BaseAdapter {
    Context mContext;
    int layoutResourceId;
    public List<SpeakerData> data;
    public List<SpeakerData> arraylist;
    DisplayImageOptions options;

    ImageDownloader mImageDownloader=new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();

  public SpeakersAdapter(Context mContext,int LayoutResourceID, List<SpeakerData> data)
    {
        this.mContext=mContext;
        this.layoutResourceId=LayoutResourceID;
        this.data=data;
        arraylist=new ArrayList<SpeakerData>();
        arraylist.addAll(data);
        if(!imageLoader.isInited()){

            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img1)
                .showImageForEmptyUri(R.drawable.img1)
                .showImageOnFail(R.drawable.img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        boolean firsttime=true;
       viewHolder mholder = null;
        if(convertView==null){
            // inflate the layout
            mholder=new viewHolder();
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, viewGroup, false);
            mholder.biography=(TextView)convertView.findViewById(R.id.biography);
            mholder.name=(TextView)convertView.findViewById(R.id.speaker_name);
            mholder.imageView=(ImageView)convertView.findViewById(R.id.speaker_image);

            convertView.setTag(mholder);

        } else {
            mholder = (viewHolder) convertView.getTag();
        }
        mholder = (viewHolder) convertView.getTag();
        mholder.biography.setText(data.get(i).biography.split(",,")[0]);
        mholder.name.setText(data.get(i).title+". "+data.get(i).name);

        final String imageName=data.get(i).image_name;

		/*check
		 * check if image came from server becaues if image came from server it save in the database with extension like
		 * yuoga.jpg or yuoga.png but if it was image from drawable using like yuoga.png we remove extension of image name and save it
		 * in db like "yuoga" no ".png" because we are getting image from drawable with string name so its remove extendion .the method used to get id is define in utils class
		 * im.setImageResource(Utils.getId(imageName)); this line for image in drawable for static purpose.......
		 * */

        if (!imageName.isEmpty()) {         //code for update image
                Bitmap bmp = mImageDownloader.getImage(imageName);
                //check if image was in cache
                if (bmp != null) {
                    mholder.imageView.setImageBitmap(bmp);

                }
                //else part if not downloaded then download it...........
                else {
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            if (imageName != null) mImageDownloader.downloadImage(imageName);
                        }
                    };
                    thread.start();
                    //in case not download as soon as thread coplete its work set dummy image
                    //mImg.setImageResource(R.drawable.gallery_test);
                    String url = AppGlobal.ServerImagePath + imageName;

                    //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                    ////////////////////
                    imageLoader.displayImage(url, mholder.imageView, options, new SimpleImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {

                                }
                            }
                    );
                }
        }else
        {
            mholder.imageView.setImageResource(R.drawable.img1);
        }
        return convertView;
    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        data.clear();
        if (charText.length() == 0) {
            data.addAll(arraylist);
        }
        else
        {
            for (SpeakerData sd : arraylist)
            {
                if (sd.speakerFullName.toLowerCase(Locale.getDefault()).contains(charText) || sd.name.toLowerCase(Locale.getDefault()).contains(charText))
                {
                    data.add(sd);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class viewHolder
    {
        ImageView imageView;
        TextView name,biography;
    }
}
