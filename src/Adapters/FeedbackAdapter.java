package Adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.brainforum.org.R;
import com.brainforum.org.SessionFeedback;

import java.util.List;

import DataStructure.FeedbackData;
import DataStructure.SpeakerData;

/**
 * Created by Muhammad Nasir on 1/26/2015.
 */
public class FeedbackAdapter extends BaseAdapter {
    Context mContext;
    int layoutResourceId;
    public List<FeedbackData> data;
    viewHolder holder;

    public FeedbackAdapter(Context mContext,int LayoutResourceID, List<FeedbackData> data)
    {
        this.mContext=mContext;
        this.layoutResourceId=LayoutResourceID;
        this.data=data;
        holder=new viewHolder();
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        boolean firsttime=true;
        viewHolder holder=null;
        if(convertView==null){
            holder = new viewHolder();
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, viewGroup, false);
            holder.question=(TextView)convertView.findViewById(R.id.feedback_question);
            holder.number=(TextView)convertView.findViewById(R.id.number_question);
            holder.rating=(RatingBar)convertView.findViewById(R.id.rating_bar);
            convertView.setTag(holder);
            if (holder.rating != null) {
                holder.rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    public void onRatingChanged(RatingBar ratingBar,
                                                float rating, boolean fromUser) {
                        if (fromUser) {

                            data.get(i).rating = rating;
                        }
                    }
                });
                holder.rating.setRating(data.get(i).rating);
            }

        } else {
            holder = (viewHolder) convertView.getTag();
        }


            try {
                LayerDrawable stars = (LayerDrawable) holder.rating.getProgressDrawable();
                stars.getDrawable(2).setColorFilter(Color.parseColor("#FFA500"), PorterDuff.Mode.SRC_ATOP);
            }catch(Exception e)
            {
                e.printStackTrace();
            }
            holder.rating.setRating(data.get(i).rating);
            holder.rating.setId(Integer.parseInt(data.get(i).id));



/*try {
    LayerDrawable stars = (LayerDrawable) holder.rating.getProgressDrawable();
    stars.getDrawable(2).setColorFilter(Color.parseColor("#FFA500"), PorterDuff.Mode.SRC_ATOP);
}catch(Exception e)
{
    e.printStackTrace();
}*/

        holder.question.setText(data.get(i).question);
        holder.number.setText(data.get(i).number);
//        holder.rating.setRating(0);
//        holder.rating.setId(Integer.parseInt(data.get(i).id));
        if (firsttime) {
            SessionFeedback.ratingBars.add(holder.rating);
            firsttime=false;
        }
        //holder.rating.setRating(Float.parseFloat(data.get(i).rating));

        return convertView;
    }
    public class viewHolder
    {
        RatingBar rating;
        TextView question,number;
    }
}
