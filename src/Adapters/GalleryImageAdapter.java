package Adapters;

/**
 * Created by Muhammad Nasir on 2/6/2015.
 */
        import java.util.ArrayList;
        import java.util.List;


        import com.brainforum.org.R;
        import com.nostra13.universalimageloader.core.DisplayImageOptions;
        import com.nostra13.universalimageloader.core.ImageLoader;
        import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
        import com.nostra13.universalimageloader.core.assist.FailReason;
        import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
        import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;


        import android.content.Context;
        import android.graphics.Bitmap;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.ImageView;
        import android.widget.ProgressBar;

        import DataStructure.AppGlobal;
        import DataStructure.GalleryData;
        import DataStructure.ImageDownloader;

public class GalleryImageAdapter extends BaseAdapter {
    private Context mContext;
    String imageName;
    List<GalleryData> mgGallery = new ArrayList<GalleryData>();
    private LayoutInflater inflater;
    DisplayImageOptions options;

    ImageDownloader mImageDownloader=new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();


    // Constructor
    public GalleryImageAdapter(Context c,List<GalleryData> data){//,List<GalleryObject> data
        mContext = c;
        mgGallery=data;

        inflater = LayoutInflater.from(mContext);
        if(!imageLoader.isInited()){

            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_1)
                .showImageForEmptyUri(R.drawable.img_1)
                .showImageOnFail(R.drawable.img_1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public int getCount() {
        return mgGallery.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder ;
        if (view == null) {
            view = inflater.inflate(R.layout.grid_view_cell, parent, false);
            holder = new ViewHolder();
            assert view != null;
            holder.imageView=(ImageView) view.findViewById(R.id.gv_cell);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        imageName=mgGallery.get(position).image_name;

		/*check
		 * check if image came from server becaues if image came from server it save in the database with extension like
		 * yuoga.jpg or yuoga.png but if it was image from drawable using like yuoga.png we remove extension of image name and save it
		 * in db like "yuoga" no ".png" because we are getting image from drawable with string name so its remove extendion .the method used to get id is define in utils class
		 * im.setImageResource(Utils.getId(imageName)); this line for image in drawable for static purpose.......
		 * */
        //code for update image
        if(imageName.contains(".png")||imageName.contains(".jpg")||imageName.contains(".JPG")){
            Bitmap bmp=mImageDownloader.getImage(imageName);
            //check if image was in cache
            if(bmp!=null){
                holder.imageView.setImageBitmap(bmp);

            }
            //else part if not downloaded then download it...........
            else{
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if(imageName!=null)mImageDownloader.downloadImage(imageName);
                    }
                };
                thread.start();
                //in case not download as soon as thread coplete its work set dummy image
                //mImg.setImageResource(R.drawable.gallery_test);

                imageName=imageName.replaceAll(" ", "%20");
                String url=AppGlobal.ServerImagePath+imageName;

                //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                ////////////////////
                imageLoader.displayImage(url, holder.imageView, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view,
                                                        FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                            }
                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current,
                                                         int total) {
                            }
                        }
                );

                /////////////////////

            }
        }
        //if it dosenot contain extension it means its was image from drawable use getid method define in util class for for geting id after giving image name.
        else{
            holder.imageView.setImageResource(AppGlobal.getResourceId(mContext,imageName,"drawable"));


        }
        return view;
    }
    class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
    }
}
