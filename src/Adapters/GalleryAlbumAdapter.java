package Adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.brainforum.org.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

import DataStructure.AppGlobal;
import DataStructure.FeedbackData;
import DataStructure.GalleryAlbumsData;
import DataStructure.ImageDownloader;

/**
 * Created by Muhammad Nasir on 2/6/2015.
 */
public class GalleryAlbumAdapter extends BaseAdapter {
    Context mContext;
    int layoutResourceId;
    public List<GalleryAlbumsData> data;
    viewHolder holder;
    DisplayImageOptions options;

    ImageDownloader mImageDownloader=new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();


    public GalleryAlbumAdapter(Context mContext, int LayoutResourceID, List<GalleryAlbumsData> data) {
        this.mContext = mContext;
        this.layoutResourceId = LayoutResourceID;
        this.data = data;
        holder = new viewHolder();
        if(!imageLoader.isInited()){

            imageLoader.init(ImageLoaderConfiguration.createDefault(mContext));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_1)
                .showImageForEmptyUri(R.drawable.img_1)
                .showImageOnFail(R.drawable.img_1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, viewGroup, false);
        }
        holder.albumImage = (ImageView) convertView.findViewById(R.id.child_album);
        holder.Description = (TextView) convertView.findViewById(R.id.album_desc);
        holder.Title = (TextView) convertView.findViewById(R.id.album_name);

        final GalleryAlbumsData obj = data.get(i);
        holder.Title.setText(obj.title);
        holder.Description.setText(Html.fromHtml(obj.description));
        Log.d("Image Name", obj.image);
       final String imageName=obj.image.replaceAll(" ", "%20");
        if(imageName.contains(".png")||imageName.contains(".jpg")){
            Bitmap bmp=mImageDownloader.getImage(imageName);
            //check if image was in cache
            if(bmp!=null){
                holder.albumImage.setImageBitmap(bmp);

            }
            //else part if not downloaded then download it...........
            else{
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if(imageName!=null)mImageDownloader.downloadImage(imageName);
                    }
                };
                thread.start();
                //in case not download as soon as thread coplete its work set dummy image
                //mImg.setImageResource(R.drawable.gallery_test);

                String url=AppGlobal.ServerImagePath+imageName;

                //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                ////////////////////
                imageLoader.displayImage(url, holder.albumImage, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view,
                                                        FailReason failReason) {
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            }
                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current,
                                                         int total) {

                            }
                        }
                );

                /////////////////////

            }
        }
        //if it dosenot contain extension it means its was image from drawable use getid method define in util class for for geting id after giving image name.
        else{
            holder.albumImage.setImageResource(AppGlobal.getResourceId(mContext,imageName,"drawable"));


        }
       // holder.albumImage.setBackgroundResource(AppGlobal.getResourceId(mContext, data.get(i).image, "drawable"));
        return convertView;
    }

    private class viewHolder {
        ImageView albumImage;
        TextView Title, id, Description;
    }
}
