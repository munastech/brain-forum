package com.brainforum.org;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Collections;

import DataStructure.ProgramData;

import static DataStructure.AppGlobal.*;

/**
 * Created by Muhammad Nasir on 1/22/2015.
 */
public class ProgramList extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.programs);


//        if (Programs==null) {
        ProgressTask pt = new ProgressTask(this);
        pt.execute(ServerName + "getProgarms/");
        /*}
        else
        {
            SetViews();
        }*/
        //SetViews();

    }

    public void SetViews() {
        TableLayout tbl = (TableLayout) findViewById(R.id.events);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (Programs != null) {
            for (int i = 0; i < Programs.size(); i++) {
                TableRow tr = (TableRow) inflater.inflate(R.layout.event_list_header, tbl, false);
                TextView date = (TextView) tr.findViewById(R.id.date);
                TextView month = (TextView) tr.findViewById(R.id.month);
                TextView day = (TextView) tr.findViewById(R.id.day);
                TextView day_count = (TextView) tr.findViewById(R.id.day_count);

                final ProgramData programObj = Programs.get(i);
                date.setText(programObj.date);
                month.setText(programObj.month);
                day.setText(programObj.DAY);
                day_count.setText("Day" + programObj.daycount);
                String ProgramDate = programObj.program_date;
                tbl.addView(tr);
                /*
                Log.d("Session array size", Sessions.size()+" size");
                */


                for (int j = 0; j < Sessions.size(); j++) {
                    final ProgramData SessionObj = Sessions.get(j);
                    if (SessionObj.program_date.equals(ProgramDate)) {
                        tr = (TableRow) inflater.inflate(R.layout.event_list_child, tbl, false);
                        TextView time = (TextView) tr.findViewById(R.id.event_time);
                        TextView eventName = (TextView) tr.findViewById(R.id.event_name);
                        TextView eventDesc = (TextView) tr.findViewById(R.id.event_desc);
                        time.setText(SessionObj.start_time + "\n" + SessionObj.end_time);
                        eventName.setText(SessionObj.program_name);
                        Spanned desc = Html.fromHtml(SessionObj.description);
                        eventDesc.setText(desc);
                        final int k = j;
                        tr.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(ProgramList.this, ProgramDesc.class);
                                /*i.putExtra("program_id",SessionObj.program_id);
                                i.putExtra("program_time",SessionObj.start_time+"\n"+SessionObj.end_time);
                                i.putExtra("program_month",programObj.month);
                                i.putExtra("program_day",programObj.DAY);
                                i.putExtra("program_date",programObj.date);
                                i.putExtra("program_title",SessionObj.program_name);
                                i.putExtra("program_desc",SessionObj.description);
                                i.putExtra("program_address",SessionObj.address);*/
                                SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("program_id", SessionObj.program_id);
                                editor.putString("program_time", SessionObj.start_time + "\n" + SessionObj.end_time);
                                editor.putString("program_month", programObj.month);
                                editor.putString("program_day", programObj.DAY);
                                editor.putString("program_date", programObj.date);
                                editor.putString("program_title", SessionObj.program_name);
                                editor.putString("program_desc", SessionObj.description);
                                editor.putString("program_address", SessionObj.address);
                                editor.commit();
                                startActivity(i);
                            }
                        });
                        tbl.addView(tr);

                    }
                }
            }
        }
    }
}
