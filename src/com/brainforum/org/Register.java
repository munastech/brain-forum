package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

import DataStructure.AppGlobal;

/**
 * Created by Muhammad Nasir on 1/19/2015.
 */
public class Register extends Activity {
    EditText EtName, EtJobTitle, EtCompany, EtContactNumber, EtBiography, EtEmail, EtPassword, EtConfirmPassword;
    String Name, JobTitle, Company, ContactNumber, Biography, Email, Password, ConfirmPassword;
    ImageView profileImage;
    // number of images to select
    private static final int PICK_IMAGE = 1;
    private Bitmap bitmap;
    CheckBox show_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        SetViews();
    }

    public void SetViews() {
        ActionBar mActionBar = getActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        RelativeLayout searchBlock = (RelativeLayout) mCustomView.findViewById(R.id.searchBlock);
        searchBlock.setVisibility(View.INVISIBLE);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        EtName = (EditText) findViewById(R.id.name);
        EtJobTitle = (EditText) findViewById(R.id.job_title);
        EtCompany = (EditText) findViewById(R.id.company);
        EtContactNumber = (EditText) findViewById(R.id.contact_number);

        EtBiography = (EditText) findViewById(R.id.biography);
        EtEmail = (EditText) findViewById(R.id.email);
        EtPassword = (EditText) findViewById(R.id.password);
        EtConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        profileImage = (ImageView) findViewById(R.id.profile_image);
        show_contact = (CheckBox) findViewById(R.id.show_contact);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });
    }

    void GetValues() {
        Name = EtName.getText().toString();
        JobTitle = EtJobTitle.getText().toString();
        Company = EtCompany.getText().toString();
        ContactNumber = EtContactNumber.getText().toString();
        Biography = EtBiography.getText().toString();
        Email = EtEmail.getText().toString();
        Password = EtPassword.getText().toString();
        ConfirmPassword = EtConfirmPassword.getText().toString();
    }


    public void Submit(View v) {
        GetValues();
        if (ValidateInputs()) {
            String contact_info = "NO";
            if (show_contact.isChecked()) {
                contact_info = "YES";
            }
            ImageUploadTask iut = new ImageUploadTask(this, bitmap, Name, JobTitle, Company, ContactNumber, Email, Password, Biography, contact_info, "add", "");
            iut.execute();
        }
      /*  ProgressTask pt=new ProgressTask(this);
        if (pt.isOnline())
            pt.execute(AppGlobal.ServerName+"registeration/",Name,JobTitle,Company,ContactNumber,Email,Password,Biography);*/
    }

    private boolean ValidateInputs() {
        if (Name.equals("")) {
            Toast.makeText(this, "Please Enter Name", Toast.LENGTH_LONG).show();
            return false;
        } else if (JobTitle.equals("")) {
            Toast.makeText(this, "Please Enter Job Title", Toast.LENGTH_LONG).show();
            return false;
        } else if (Company.equals("")) {
            Toast.makeText(this, "Please Enter Company", Toast.LENGTH_LONG).show();
            return false;
        } else if (ContactNumber.equals("")) {
            Toast.makeText(this, "Please Enter Contact Number", Toast.LENGTH_LONG).show();
            return false;
        } else if (Biography.equals("")) {
            Toast.makeText(this, "Please Enter Biography", Toast.LENGTH_LONG).show();
            return false;
        } else if (Email.equals("")) {
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_LONG).show();
            return false;
        } else if (Password.equals("")) {
            Toast.makeText(this, "Please Enter Password", Toast.LENGTH_LONG).show();
            return false;
        } else if (ConfirmPassword.equals("") || !ConfirmPassword.equals(Password)) {
            Toast.makeText(this, "Password do not match", Toast.LENGTH_LONG).show();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            Toast.makeText(this, "Email is not valid", Toast.LENGTH_LONG).show();
            return false;
        } else
            return true;
    }

    /**
     * Opens dialog picker, so the user can select image from the gallery. The
     * result is returned in the method <code>onActivityResult()</code>
     */
    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.d("Image path", picturePath);
            cursor.close();

            bitmap = AppGlobal.decodeFile(picturePath, profileImage);

        }
    }

}
