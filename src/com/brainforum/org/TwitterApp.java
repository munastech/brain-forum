package com.brainforum.org;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.api.TimelinesResources;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Muhammad Nasir on 2/25/2015.
 */
public class TwitterApp extends BaseActivity implements View.OnClickListener {
    private static final String PREF_NAME = "sample_twitter_pref";
    static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    SharedPreferences mSharedPreferences;
    String url;
    WebView twitter;
    TextView official, unofficial;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter);
        prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
        url = prefs.getString("twitter_official", "");
        SetViews();

        ShowTweets();
    }

    private void SetViews() {
        twitter = (WebView) findViewById(R.id.webview);
        official = (TextView) findViewById(R.id.official);
        unofficial = (TextView) findViewById(R.id.unofficial);
        official.setOnClickListener(this);
        unofficial.setOnClickListener(this);
        LoadPages();
    }

    private void LoadPages() {
        WebSettings settings = twitter.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        twitter.loadUrl(url);
        twitter.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                getWindow().setTitle(title); //Set Activity tile to page title.
            }
        });

        twitter.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
    }

    public void ShowTweets() {
        /* // The factory instance is re-useable and thread safe.
        final ConfigurationBuilder builder = new ConfigurationBuilder().
                setOAuthConsumerKey(getString(R.string.twitter_consumer_key)).
                setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
                .setOAuthAccessToken("204222341-xYnsriYbjupa0uftA2emULK5DvPhE3IUJIfckqxy")
                .setOAuthAccessTokenSecret("9N1cJLCxyFKEtkiqEf3Cut2QXBmDdFULvBxSxdVJqN2EE");
         *//* Initialize application preferences *//*
        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);
        // Access Token
        String access_token = mSharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
        // Access Token Secret
        String access_token_secret = mSharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

        AccessToken accessToken = new AccessToken(access_token, access_token_secret);
        Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);
       // final Configuration configuration = builder.build();
        //final TwitterFactory factory = new TwitterFactory(configuration);
        //Twitter sing = twitter.getSingleton();
       // TimelinesResources response=twitter.timelines();

        List<Status> statuses = null;
        try {
            statuses = twitter.getHomeTimeline();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        System.out.println("Showing home timeline.");
        for (Status status : statuses) {
            System.out.println(status.getUser().getName() + ":" +
                    status.getText());
        }*/
    }

    @Override
    public void onClick(View view) {
        //Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_LONG).show();
        if (view.getId() == R.id.official) {
            url = prefs.getString("twitter_official", "");
        } else {
            url = prefs.getString("twitter_unofficial", "");
        }
        LoadPages();
    }
}
