package com.brainforum.org;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Adapters.FeedbackAdapter;
import DataStructure.AppGlobal;
import DataStructure.FeedbackData;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class SessionFeedback extends BaseActivity {
    FeedbackAdapter adapter;
    ListView sessionList;
    public static List<RatingBar> ratingBars;
    TextView session_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.session_feedback);
        Intent i = getIntent();
        String id = i.getStringExtra("session_id");
        String title = i.getStringExtra("session_title");
        session_title = (TextView) findViewById(R.id.session_title);
        session_title.setText(title);
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getQuestion/?program_id=" + id);
        // SetViews();
    }

    public void SetViews() {
        if (AppGlobal.FeedbackList != null) {
            ratingBars = new ArrayList<RatingBar>();
            sessionList = (ListView) findViewById(R.id.feedbackList);
            adapter = new FeedbackAdapter(this, R.layout.feedback_child, AppGlobal.FeedbackList);
            sessionList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public void SubmitFeedback(View v) {
        String QIds = "";
        String Rating = "";
        for (int i = 0; i < ratingBars.size(); i++) {
            QIds += ratingBars.get(i).getId() + ",";
            Rating += ratingBars.get(i).getRating() + ",";
        }
        assert QIds != null;
        QIds = QIds.replaceAll(" ,$", "");
        Rating = Rating.replaceAll(" ,$", "");
        ProgressTask pt = new ProgressTask(SessionFeedback.this);
        pt.execute(AppGlobal.ServerName + "addFeedback/?question_id=" + QIds + "&feedback=" + Rating + "&user_id=" + AppGlobal.userId, "feeds");
        //Toast.makeText(SessionFeedback.this,"Qids/ratings "+QIds+"/"+Rating,Toast.LENGTH_LONG).show();
      /*  Intent i=new Intent(this,FeedbackResponse.class);
        startActivity(i);*/
    }
}
