package com.brainforum.org;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import DataStructure.AppGlobal;
import DataStructure.FactoryApplication;
import DataStructure.ImageDownloader;
import DataStructure.ProgramData;
import DataStructure.SpeakerData;

/**
 * Created by Muhammad Nasir on 1/26/2015.
 */
public class SpeakerDesc extends BaseActivity {
    TextView speaker_title, speakerName, email;
    LinearLayout biography;
    ImageView speakerImage;
    LinearLayout speakingAt;
    int position;
    DisplayImageOptions options;
    boolean fromEvents = false;
    ImageDownloader mImageDownloader = new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();
    SpeakerData obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speaker_desc);
        Intent i = getIntent();
        position = i.getIntExtra("position", 0);
        fromEvents = i.getBooleanExtra("fromEvents", false);
        if (fromEvents) obj = AppGlobal.EventSpeakers.get(position);
        else
            obj = AppGlobal.SpeakersList.get(position);
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getSpeakerProgram/" + obj.id);

        if (!imageLoader.isInited()) {

            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img1)
                .showImageForEmptyUri(R.drawable.img1)
                .showImageOnFail(R.drawable.img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        SetViews();
        SetValues();


    }

    private void SetViews() {
        speakerName = (TextView) findViewById(R.id.speaker_name);
        speaker_title = (TextView) findViewById(R.id.speaker_title);
        speakerImage = (ImageView) findViewById(R.id.speaker_image);
        biography = (LinearLayout) findViewById(R.id.biography);
        email = (TextView) findViewById(R.id.email);
        speakingAt = (LinearLayout) findViewById(R.id.speakingAt);
    }

    private void SetValues() {
        speakerName.setText(obj.name);
        //biography.setText(AppGlobal.SpeakersList.get(position).biography);
        email.setText(obj.contact);
        speaker_title.setText(obj.title + ". ");
        String[] bioArr = obj.biography.split(",,");
        for (int j = 0; j < bioArr.length; j++) {
            LayoutInflater inflater = getLayoutInflater();
            LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.bio_child, biography, false);
            TextView bioTV = (TextView) ll.findViewById(R.id.biography);
            bioTV.setText(bioArr[j]);
            biography.addView(ll);
        }
        final String imageName = obj.image_name;
        Log.d("Image Name",imageName);
            Bitmap bmp = mImageDownloader.getImage(imageName);
            //check if image was in cache
            if (bmp != null) {
                Log.d("Image Available", "Yes");
                speakerImage.setImageBitmap(bmp);

            }
            //else part if not downloaded then download it...........
            else {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if (imageName != null) mImageDownloader.downloadImage(imageName);
                    }
                };
                thread.start();
                //in case not download as soon as thread coplete its work set dummy image
                //mImg.setImageResource(R.drawable.gallery_test);
                String url = AppGlobal.ServerImagePath + imageName;

                //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                ////////////////////
                imageLoader.displayImage(url, speakerImage, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }
                        }
                );
            }
    }

    public void GenerateEventsList() {
        LayoutInflater inflater = getLayoutInflater();
        for (int i = 0; i < AppGlobal.SpeakerEvents.size(); i++) {
            LinearLayout ll = (LinearLayout) inflater.inflate(R.layout.speaker_event_child, speakingAt, false);
            TextView event_name = (TextView) ll.findViewById(R.id.event_name);
            TextView event_des = (TextView) ll.findViewById(R.id.event_des);
            TextView event_time = (TextView) ll.findViewById(R.id.event_time);

            final ProgramData obj = AppGlobal.SpeakerEvents.get(i);
            event_name.setText(obj.program_name);
            event_des.setText(obj.description);
            event_time.setText(obj.date + " " + obj.month + " | " + obj.start_time + " - " + obj.end_time);
            speakingAt.addView(ll);

            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SpeakerDesc.this, ProgramDesc.class);
                    intent.putExtra("program_id", obj.program_id);
                    intent.putExtra("program_time", obj.start_time + "\n" + obj.end_time);
                    intent.putExtra("program_month", obj.month);
                    intent.putExtra("program_day", obj.DAY);
                    intent.putExtra("program_date", obj.date);
                    intent.putExtra("program_title", obj.program_name);
                    intent.putExtra("program_desc", obj.description);
                    startActivity(intent);
                    SpeakerDesc.this.finish();
                }
            });


        }
    }
}
