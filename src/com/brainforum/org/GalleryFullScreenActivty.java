package com.brainforum.org;

import android.os.Bundle;

/**
 * Created by Muhammad Nasir on 2/6/2015.
 */
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;

import Adapters.GalleryImagePagerAdapter;
import DataStructure.AppGlobal;
import DataStructure.GalleryData;

public class GalleryFullScreenActivty extends BaseActivity {
    List<GalleryData> mgGallery = AppGlobal.galleryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.galleryfulllscreen);
        Bundle extras = getIntent().getExtras();
        int id = extras.getInt("position");
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        if (mgGallery.size() > 0) {
            GalleryImagePagerAdapter adapter = new GalleryImagePagerAdapter(GalleryFullScreenActivty.this, mgGallery);
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(id);
        }


		/*Log.e("id button : ", id+"");*/

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}

