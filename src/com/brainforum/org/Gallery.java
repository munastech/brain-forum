package com.brainforum.org;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import Adapters.GalleryImageAdapter;
import DataStructure.AppGlobal;
import DataStructure.FactoryApplication;
import DataStructure.GalleryData;

/**
 * Created by Muhammad Nasir on 2/6/2015.
 */
public class Gallery extends BaseActivity {

    GridView galleryView;
    GalleryImageAdapter adapter;
    String album_id, album_title, album_desc, album_image;
    ImageView album_img;
    TextView title, desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery);
        GetIntentValues();
        SetViews();
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getGallerybyID/" + album_id);

        // SetViews();
    }

    private void GetIntentValues() {
        Intent intent = getIntent();
        album_id = intent.getStringExtra("album_id");
        album_title = intent.getStringExtra("album_title");
        album_desc = intent.getStringExtra("album_desc");
        album_image = intent.getStringExtra("album_image");
    }

    private void SetViews() {
        galleryView = (GridView) findViewById(R.id.gallery_grid);
        album_img = (ImageView) findViewById(R.id.album_image);
        desc = (TextView) findViewById(R.id.album_desc);
        title = (TextView) findViewById(R.id.album_name);
        desc.setText(Html.fromHtml(album_desc));
        title.setText(album_title);
       /* try{
            AppGlobal.decodeFile(FactoryApplication.getAppContext().getExternalFilesDir(null) + "/BrainForum/Cache/" + album_image,album_img);
        }catch (Exception e)
        {
            e.printStackTrace();
        }*/
        new AsyncTask<Void, Void, Void>() {
            Drawable img = null;

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                album_img.setImageDrawable(img);

            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    img = AppGlobal.grabImageFromUrl(AppGlobal.ServerImagePath + album_image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();

    }


    public void GenerateList() {
        adapter = new GalleryImageAdapter(this, AppGlobal.galleryList);
        galleryView.setAdapter(adapter);
        galleryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Gallery.this, GalleryFullScreenActivty.class);
                intent.putExtra("position", i);
                startActivity(intent);
            }
        });
    }
}

