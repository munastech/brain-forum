package com.brainforum.org;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by Harris Rashid on 6/23/14.
 */
public class MyDialog {

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static void showMessage(String title, String message, final Activity activity) {

  /*      if (both) {
            AlertDialog.Builder customBuilder = new AlertDialog.Builder(activity)
                    .setTitle(title)
//                    .setIcon(R.drawable.setup_icon)
                    // .setMessage(message)

                   *//*  .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            activity.finish();
                        }
                    })
                   .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })*//*;
            LinearLayout layout = new LinearLayout(activity);
            TextView tvMessage = new TextView(activity);
            TextView tvline = new TextView(activity);

            Button bt = new Button(activity);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams btlayoutParams = new LinearLayout.LayoutParams(200, 70);
            LinearLayout.LayoutParams tvlayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 2);

            layout.setLayoutParams(layoutParams);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setGravity(Gravity.CENTER);
            layout.setPadding(0, 5, 0, 0);

            tvMessage.setMinHeight(300);
            tvMessage.setText(message);
            tvMessage.setPadding(25, 5, 5, 5);
            //tvMessage.setGravity(Gravity.CENTER);

            tvline.setBackgroundColor(Color.parseColor("#000000"));
            tvline.setLayoutParams(tvlayoutParams);

            btlayoutParams.setMargins(0, 20, 0, 10);

            final String activityName = activity.getLocalClassName();
            if (activityName.equalsIgnoreCase("Setup")) {
                bt.setBackgroundResource(R.drawable.finish_bt_m);
            } else {

                bt.setBackgroundResource(R.drawable.all_buttons_bg);
                bt.setText("OK");
            }
            bt.setLayoutParams(btlayoutParams);
            bt.setPadding(0, 5, 0, 0);
            bt.setTextColor(Color.parseColor("#ffffff"));

            layout.addView(tvMessage);
            layout.addView(tvline);
            layout.addView(bt);


            customBuilder.setView(layout);
            final AlertDialog ab = customBuilder.create();
            ab.show();
            bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("Activity naME", activityName);
                    if (activityName.equals("AlarmMedList")) {
                        Intent i = new Intent(activity, MainActivity.class);
                        SharedPreferences prefs = activity.getSharedPreferences("client_file", activity.MODE_PRIVATE);
                        prefs.edit().putString("ButtonClicked", "").commit();
                        activity.startActivity(i);

                        activity.finish();
                    } else {
                        Intent i = new Intent(activity, Login.class);
                        activity.startActivity(i);
                        activity.finish();
                    }
                    ab.dismiss();
                }

            });

           *//* ab.getWindow().getAttributes();
            Button b = ab.getButton(DialogInterface.BUTTON_POSITIVE);
            if(b != null)
                b.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.all_buttons_bg));
            b.setTextSize(50);
            b.setWidth(5);*//*
        } else */
        {
            new AlertDialog.Builder(activity)
                    .setTitle(title)
                            //.setIcon(R.drawable.setup_icon)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //do nothing
                        }
                    })
                    .show();
        }
    }
}
