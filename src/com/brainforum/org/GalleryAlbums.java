package com.brainforum.org;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import Adapters.GalleryAlbumAdapter;
import DataStructure.AppGlobal;
import DataStructure.GalleryAlbumsData;

/**
 * Created by Muhammad Nasir on 2/6/2015.
 */
public class GalleryAlbums extends BaseActivity {

    ListView LWalbum;
    List<GalleryAlbumsData> list;
    GalleryAlbumAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.albums);
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getAlbum");
        // SetViews();
    }

    public void SetViews() {
        list = AppGlobal.GalleryAlbumList;
        LWalbum = (ListView) findViewById(R.id.albums);
        adapter = new GalleryAlbumAdapter(this, R.layout.album_child, list);
        LWalbum.setAdapter(adapter);

        LWalbum.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(GalleryAlbums.this, Gallery.class);
                intent.putExtra("album_id", AppGlobal.GalleryAlbumList.get(i).id);
                intent.putExtra("album_image", AppGlobal.GalleryAlbumList.get(i).image);
                intent.putExtra("album_title", AppGlobal.GalleryAlbumList.get(i).title);
                intent.putExtra("album_desc", AppGlobal.GalleryAlbumList.get(i).description);
                startActivity(intent);
            }
        });
        adapter.notifyDataSetChanged();

    }
}
