package com.brainforum.org;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import DataStructure.AppGlobal;
import DataStructure.ImageDownloader;

/**
 * Created by Muhammad Nasir on 1/22/2015.
 */
public class About extends BaseActivity {
    private Context mContext;
    ViewFlipper mViewFlipper;
    ScrollView moverHandle;
    private Animation.AnimationListener mAnimationListener;
    TextView aboutContent, mission;
    DisplayImageOptions options;

    ImageDownloader mImageDownloader = new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.about);
        if (AppGlobal.About == null) {
            ProgressTask pt = new ProgressTask(this);
            pt.execute(AppGlobal.ServerName + "aboutUs");
        } else {
            SettingData();
        }

        moverHandle = (ScrollView) findViewById(R.id.aboutScrollView);
        moverHandle.setOnTouchListener(new SwipeListView() {
            public void onSwipeRight() {
                //Toast.makeText(About.this, "right", Toast.LENGTH_SHORT).show();
                mViewFlipper.showPrevious();
            }

            public void onSwipeLeft() {
                // Toast.makeText(About.this, "left", Toast.LENGTH_SHORT).show();
                mViewFlipper.showNext();
            }
        });
        if (!imageLoader.isInited()) {

            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.image1)
                .showImageForEmptyUri(R.drawable.image1)
                .showImageOnFail(R.drawable.image1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
        // SettingData();
    }

    public void SettingData() {
        aboutContent = (TextView) findViewById(R.id.aboutContent);
        mission = (TextView) findViewById(R.id.mission);
        mViewFlipper = (ViewFlipper) this.findViewById(R.id.view_flipper);
        if (AppGlobal.About != null && !AppGlobal.About.equals("")) {
            String[] AboutArr = AppGlobal.About.split("@@");
            Spanned about = Html.fromHtml(AboutArr[0]);
            aboutContent.setText(about);
            if (AboutArr.length > 1) {
                Spanned missionText = Html.fromHtml(AboutArr[1]);
                mission.setText(missionText);
            }
        }
        if (AppGlobal.AboutImages != null) {
            mViewFlipper.removeAllViews();
            for (int i = 0; i < AppGlobal.AboutImages.size(); i++) {
                ImageView image = new ImageView(this);
                //image.setBackgroundResource(res);

                final String imageName = AppGlobal.AboutImages.get(i);
                if (imageName.contains(".png") || imageName.contains(".jpg") || imageName.contains(".JPG")) {
                    Bitmap bmp = mImageDownloader.getImage(imageName);
                    //check if image was in cache
                    if (bmp != null) {
                        Log.d("Image Available", "Yes");
                        image.setImageBitmap(bmp);

                    }
                    //else part if not downloaded then download it...........
                    else {
                        Thread thread = new Thread() {
                            @Override
                            public void run() {
                                if (imageName != null) mImageDownloader.downloadImage(imageName);
                            }
                        };
                        thread.start();
                        //in case not download as soon as thread coplete its work set dummy image
                        //mImg.setImageResource(R.drawable.gallery_test);
                        String url = AppGlobal.ServerImagePath + imageName;

                        //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                        ////////////////////
                        imageLoader.displayImage(url, image, options, new SimpleImageLoadingListener() {
                                    @Override
                                    public void onLoadingStarted(String imageUri, View view) {

                                    }
                                }
                        );
                    }
                }
                //if it dosenot contain extension it means its was image from drawable use getid method define in util class for for geting id after giving image name.
                else {
                    image.setImageResource(AppGlobal.getResourceId(this, imageName, "drawable"));
                }
                mViewFlipper.addView(image);
            }
        }
        SetAnimation();
    }


    public void SetAnimation() {
        mViewFlipper.setAutoStart(true);
        mViewFlipper.setFlipInterval(4000);
        mViewFlipper.startFlipping();
        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_in));
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_out));
        // controlling animation
        mViewFlipper.getInAnimation().setAnimationListener(mAnimationListener);
        mViewFlipper.showNext();
        //animation listener
        mAnimationListener = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                //animation started event
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                //TODO animation stopped event
            }
        };
    }


}
