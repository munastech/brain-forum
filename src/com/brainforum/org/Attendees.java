package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Locale;

import Adapters.AttendeeAdapter;
import DataStructure.AppGlobal;
import DataStructure.AttendeeData;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class Attendees extends BaseActivity {
    AttendeeAdapter adapter;
    ListView AttendeeList;
    EditText editsearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendees);
        AttendeeList = (ListView) findViewById(R.id.attendeesList);
//        if (AppGlobal.AttendeesList==null) {
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getAttendee");
        /*}else
        {
            SetViews();
        }*/
        Search();
    }

    public void Search() {
        ManageActionBar();
        // Locate the EditText in listview_main.xml
        editsearch = (EditText) findViewById(R.id.searchBar);

        // Capture Text in EditText
        editsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void ManageActionBar() {
        ActionBar mActionBar = getActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        editsearch = (EditText) mCustomView.findViewById(R.id.searchBar);
        ImageView search = (ImageView) mCustomView.findViewById(R.id.search);
        ImageView lineBlue = (ImageView) mCustomView.findViewById(R.id.lineBlue);
        ImageButton bUp = (ImageButton) mCustomView.findViewById(R.id.btn_up);
        bUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        lineBlue.setVisibility(View.VISIBLE);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editsearch.setVisibility(View.VISIBLE);
            }
        });
        search.setVisibility(View.VISIBLE);
        mActionBar.setCustomView(mCustomView);
        new ActionBar.LayoutParams(120, 220);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    public void SetViews() {
        if (AppGlobal.AttendeesList != null) {
            adapter = new AttendeeAdapter(this, R.layout.attendee_child, AppGlobal.AttendeesList);
            AttendeeList.setAdapter(adapter);
            AttendeeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(Attendees.this, AttendeeDesc.class);
                    intent.putExtra("position", i);
                    startActivity(intent);
                }
            });
        }

    }
}
