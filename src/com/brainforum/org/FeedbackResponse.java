package com.brainforum.org;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.facebook.TestSession;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class FeedbackResponse extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_response);
        Intent i = getIntent();
        String message = i.getStringExtra("message");
        TextView Etmessage = (TextView) findViewById(R.id.message);
        Etmessage.setText(message);
    }

    public void CloseResponse(View v) {
        Intent i = new Intent(this, Home.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        this.finish();
    }
}
