package com.brainforum.org;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import DataStructure.AppGlobal;
import DataStructure.NotificationData;

/**
 * Created by Muhammad Nasir on 2/10/2015.
 */
public class Notifications extends BaseActivity {

    TableLayout table_notifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);

        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "notifications?user_id=" + AppGlobal.userId);
        //SetViews();
    }

    public void SetViews() {
        table_notifications = (TableLayout) findViewById(R.id.table_notifications);
        if (AppGlobal.NotificationList != null) {
            LayoutInflater inflater = getLayoutInflater();
            for (int i = 0; i < AppGlobal.NotificationList.size(); i++) {
                final NotificationData obj = AppGlobal.NotificationList.get(i);
                final TableRow tr = (TableRow) inflater.inflate(R.layout.notification_child, table_notifications, false);
                TextView title = (TextView) tr.findViewById(R.id.title);
                TextView time = (TextView) tr.findViewById(R.id.time);
                ImageView delete = (ImageView) tr.findViewById(R.id.delete);
                final TextView description = (TextView) tr.findViewById(R.id.content);
                title.setText(obj.title);
                time.setText(obj.start_time + "-" + obj.end_date);
                description.setText(obj.description);
                final boolean[] isShowing = {false};
                tr.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isShowing[0]) {
                            description.setVisibility(View.VISIBLE);
                            isShowing[0] = true;
                        } else {
                            description.setVisibility(View.GONE);
                            isShowing[0] = false;
                        }
                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Notifications.this);
                        builder.setTitle("Alert");
                        builder.setMessage("Are you sure you want to delete ?");
                        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                table_notifications.removeView(tr);
                                ProgressTask pt = new ProgressTask(Notifications.this);
                                pt.execute(AppGlobal.ServerName + "del_notification?user_id=" + AppGlobal.userId + "&notification_id=" + obj.id, "delete");
                            }
                        });
                        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                return;
                            }
                        });
                        builder.show();

                    }
                });
                table_notifications.addView(tr);
            }
        }


    }
}
