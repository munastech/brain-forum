package com.brainforum.org;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import DataStructure.AppGlobal;
import DataStructure.AttendeeData;
import DataStructure.FactoryApplication;
import DataStructure.ImageDownloader;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class AttendeeDesc extends BaseActivity {
    TextView attendee_name, biography, email, phone_number, job_title, company_name;
    ImageView attendeeImage;
    LinearLayout phone_block;
    Intent i;
    DisplayImageOptions options;

    ImageDownloader mImageDownloader = new ImageDownloader();
    protected ImageLoader imageLoader = ImageLoader.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.attendee_desc);
        if (!imageLoader.isInited()) {

            imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img1)
                .showImageForEmptyUri(R.drawable.img1)
                .showImageOnFail(R.drawable.img1)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        SetView();
    }

    private void SetView() {
        attendee_name = (TextView) findViewById(R.id.attendee_name);
        biography = (TextView) findViewById(R.id.biography);
        email = (TextView) findViewById(R.id.email);
        phone_number = (TextView) findViewById(R.id.phone_number);
        job_title = (TextView) findViewById(R.id.job_title);
        company_name = (TextView) findViewById(R.id.company_name);
        attendeeImage = (ImageView) findViewById(R.id.attendee_image);
        phone_block = (LinearLayout) findViewById(R.id.phone_block);

        SetValues();
    }

    private void SetValues() {
        i = getIntent();
        int position = i.getIntExtra("position", 0);
        AttendeeData obj = AppGlobal.AttendeesList.get(position);
        attendee_name.setText(obj.name);
        biography.setText(Html.fromHtml(obj.biography), TextView.BufferType.SPANNABLE);
        email.setText(obj.email);
        phone_number.setText(obj.phone_number);
        job_title.setText(obj.job_title);
        company_name.setText(obj.company_name);
        if (obj.contact_info.equals("YES")) {
            phone_block.setVisibility(View.VISIBLE);
        } else if (obj.contact_info.equals("NO")) {
            phone_block.setVisibility(View.GONE);
        }
        final String imageName = obj.image;
        if (imageName.contains(".png") || imageName.contains(".jpg") || imageName.contains(".JPG")) {
            Bitmap bmp = mImageDownloader.getImage(imageName);
            //check if image was in cache
            if (bmp != null) {
                Log.d("Image Available", "Yes");
                attendeeImage.setImageBitmap(bmp);

            }
            //else part if not downloaded then download it...........
            else {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if (imageName != null) mImageDownloader.downloadImage(imageName);
                    }
                };
                thread.start();
                //in case not download as soon as thread coplete its work set dummy image
                //mImg.setImageResource(R.drawable.gallery_test);
                String url = AppGlobal.ServerImagePath + imageName;

                //UrlImageViewHelper.setUrlDrawable(mImg, url, R.drawable.gallery_test);

                ////////////////////
                imageLoader.displayImage(url, attendeeImage, options, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }
                        }
                );
            }
        }
        //if it dosenot contain extension it means its was image from drawable use getid method define in util class for for geting id after giving image name.
        else {
            attendeeImage.setImageResource(AppGlobal.getResourceId(this, imageName, "drawable"));
        }


        //attendeeImage.se(obj.name);

    }
}
