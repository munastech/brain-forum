package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import DataStructure.AppGlobal;

public class LoginActivity extends Activity {
    private Button loginBtn;
    Button login;
    private LoginButton facebook;
    private UiLifecycleHelper uiHelper;
    EditText username, password;
    CheckBox remember_me;
    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";

    /* Any number for uniquely distinguish your request */
    public static final int WEBVIEW_REQUEST_CODE = 100;

    private ProgressDialog pDialog;

    private static Twitter twitter;
    private static RequestToken requestToken;

    private static SharedPreferences mSharedPreferences;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    public boolean isTwitterClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);
        SetViews();
        ManageActionBar();

        setLoginFacebook();

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
        facebook.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if (user != null) {
                    SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
                    prefs.edit().putString("name", user.getName()).commit();
                    ProgressTask pt = new ProgressTask(LoginActivity.this);
                    pt.execute(AppGlobal.ServerName + "socialLinkLogin/?email=" + user.getId().replace(" ", "") + "&type=fb");

                    //  Toast.makeText(LoginActivity.this, "Logged In as name/id/username" + user.getName()+"/"+user.getId()+"/"+user.getUsername(), Toast.LENGTH_SHORT).show();
                } else {
                    // Toast.makeText(LoginActivity.this, "Logged out", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void ForgotPassword(View v)
    {
        Intent i=new Intent(this,ForgotPassword.class);
        startActivity(i);
    }
    private void SetViews() {
        CheckTwitterLogin();
        login = (Button) findViewById(R.id.login);
        facebook = (LoginButton) findViewById(R.id.facebook);
        remember_me=(CheckBox)findViewById(R.id.remember_me);
        //twitter = (Twitter) findViewById(R.id.twitter);
        username = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        Button forgotPassword=(Button)findViewById(R.id.forgotPassword);
        forgotPassword.setText(Html.fromHtml("<u>Forgot Password</u>"));
        findViewById(R.id.twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginToTwitter();
            }
        });
       final SharedPreferences prefs=getSharedPreferences("my_prefs",MODE_PRIVATE);
        username.setText(prefs.getString("username",""));
        password.setText(prefs.getString("password",""));
        remember_me.setChecked(prefs.getBoolean("checked",false));
        // facebook.setBackgroundResource(R.drawable.facebook_bt);

    }

    private void CheckTwitterLogin() {
        /* initializing twitter parameters from string.xml */
        initTwitterConfigs();

		/* Enabling strict mode */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        /* Initialize application preferences */
        mSharedPreferences = getSharedPreferences(PREF_NAME, 0);

        boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);


		/*  if already logged in, then hide login layout and show share layout */
        if (isLoggedIn) {

        } else {

            Uri uri = getIntent().getData();

            if (uri != null && uri.toString().startsWith(callbackUrl)) {

                String verifier = uri.getQueryParameter(oAuthVerifier);
                Log.d("Verifier", verifier + " verifier");
                try {

					/* Getting oAuth authentication token */
                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

					/* Getting user id form access token */
                    long userID = accessToken.getUserId();
                    final User user = twitter.showUser(userID);
                    final String username = user.getName();


					/* save updated token */
                    saveTwitterInfo(accessToken);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


    }

    private void saveTwitterInfo(AccessToken accessToken) {

        long userID = accessToken.getUserId();

        User user;
        try {
            user = twitter.showUser(userID);

            String username = user.getName();

            Log.d("Twitter Username /id", username + "/" + user.getId());

			/* Storing oAuth tokens to shared preferences */
            SharedPreferences.Editor e = mSharedPreferences.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            e.putString(PREF_USER_NAME, username);
            e.commit();

        } catch (TwitterException e1) {
            e1.printStackTrace();
        }
    }

    private void loginToTwitter() {
        isTwitterClicked = true;
        boolean isLoggedIn = mSharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);
        if (android.os.Build.VERSION.SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        if (!isLoggedIn) {
            final ConfigurationBuilder builder = new ConfigurationBuilder().
                    setOAuthConsumerKey(consumerKey).
                    setOAuthConsumerSecret(consumerSecret);
            //.setOAuthAccessToken("204222341-xYnsriYbjupa0uftA2emULK5DvPhE3IUJIfckqxy")
            //.setOAuthAccessTokenSecret("9N1cJLCxyFKEtkiqEf3Cut2QXBmDdFULvBxSxdVJqN2EE");

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

           /* Thread thread = new Thread(new Runnable(){
                @Override
                public void run() {
                    try {
                       // twitter.getOAuthRequestToken();
                        requestToken = twitter
                                .getOAuthRequestToken(callbackUrl);
                        LoginActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                .parse(requestToken.getAuthenticationURL())));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();*/
            try {
                requestToken = twitter.getOAuthRequestToken(callbackUrl);
                /**
                 Loading twitter login page on webview for authorization
                 Once authorized, results are received at onActivityResult

                 Loading twitter login page on webview for authorization
                 Once authorized, results are received at onActivityResult
                 */
                final Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, WEBVIEW_REQUEST_CODE);


            } catch (TwitterException e) {
                e.printStackTrace();
            }
        } else {

        }
    }

    /* Reading twitter essential configuration parameters from strings.xml */
    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }

    public void Login(View v) {
        String Email = username.getText().toString();
        String Password = password.getText().toString();
        if (Email.equals("")) {
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_LONG).show();
            return;
        }
        if (Password.equals("")) {
            Toast.makeText(this, "Please Enter Password", Toast.LENGTH_LONG).show();
            return;
        }
        SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
        String device_id = prefs.getString("gcm_id", "");

                SharedPreferences.Editor editor=prefs.edit();
                if (remember_me.isChecked())
                {
                    editor.putString("username",username.getText().toString());
                    editor.putString("password",password.getText().toString());
                    editor.putBoolean("checked",true);
                    editor.commit();
                }else{
                    editor.putString("username","");
                    editor.putString("password","");
                    editor.putBoolean("checked",false);
                    editor.commit();
                }

        ProgressTask pt = new ProgressTask(this);
        if (pt.isOnline())
            pt.execute(AppGlobal.ServerName + "login/?email=" + Email + "&password=" + Password + "&type=attendee&platform=android&device_id=" + device_id);
    }

    private void ManageActionBar() {
        ActionBar mActionBar = getActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        RelativeLayout searchBlock = (RelativeLayout) mCustomView.findViewById(R.id.searchBlock);
        searchBlock.setVisibility(View.INVISIBLE);
        mActionBar.setCustomView(mCustomView);
        new ActionBar.LayoutParams(120, 220);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    public void RegisterNow(View v) {
        Intent i = new Intent(this, Register.class);
        startActivity(i);
    }

    public void GoGuestMenu(View v) {
        Intent i = new Intent(this, Home.class);
        startActivity(i);
       /* DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        int dens=dm.densityDpi;
        double wi=(double)width/(double)dens;
        double hi=(double)height/(double)dens;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        double screenInches = Math.sqrt(x+y);
        Log.d("width/height,wi/hi,x/y",width+"/"+height+","+wi+"/"+hi+","+x+"/"+y);
        Log.d("width of res",login.getWidth()+"/"+facebook.getWidth()+","+twitter.getWidth());*/
    }

    private void setLoginFacebook() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.brainforum.org",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    /* @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.main, menu);
         MenuItem searchItem = menu.findItem(R.id.action_settings);
         TextView searchView = (TextView) MenuItemCompat.getActionView(searchItem);
         return true;
     }*/
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            onSessionStateChange(session, state, exception);
            if (state.isOpened()) {
                //Toast.makeText(LoginActivity.this,"Session state="+state,Toast.LENGTH_LONG).show();
            }

        }
    };

    private void onSessionStateChange(Session session, SessionState state,
                                      Exception exception) {
        //  final TextView name = (TextView) findViewById(R.id.name);
        //  final TextView gender = (TextView) findViewById(R.id.gender);
        // final TextView location = (TextView) findViewById(R.id.location);
        // When Session is successfully opened (User logged-in)
        if (state.isOpened()) {
            Log.i("ss", "Logged in...");
            // make request to the /me API to get Graph user
            Request.newMeRequest(session, new Request.GraphUserCallback() {

                // callback after Graph API response with user
                // object
                @Override
                public void onCompleted(GraphUser user, Response response) {
                    if (user != null) {
                       /* isFbLogin = true;*/
                        user.getId();
                        /*userId = fbUserId;*/
                        /*new LoginAsyncTask().execute();*/
                        //  Toast.makeText(LoginActivity.this, "Logged Int" + user.getName(), Toast.LENGTH_SHORT);
                        // Set view visibility to true
                        //          otherView.setVisibility(View.VISIBLE);
                        // Set User name
                        //         name.setText("Hello " + user.getName());
                        // Set Gender
                        //       gender.setText("Your Gender: "
                        //              + user.getProperty("gender").toString());
                        //     location.setText("Your Current Location: "
                        //            + user.getLocation().getProperty("name")
                        //           .toString());
                    }
                }
            }).executeAsync();
        } else if (state.isClosed()) {
            Log.i("Loggedout", "Logged out...");
            //    otherView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }
        uiHelper.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!isTwitterClicked) {
            uiHelper.onActivityResult(requestCode, resultCode, data);
        } else {
            if (resultCode == Activity.RESULT_OK) {
                String verifier = data.getExtras().getString(oAuthVerifier);
                try {
                    AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);

                    long userID = accessToken.getUserId();
                    final User user = twitter.showUser(userID);
                    String username = user.getScreenName().replace(" ", "");
                    String name = user.getName();
                    SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
                    prefs.edit().putString("name", name).commit();
                    saveTwitterInfo(accessToken);
                    Log.e("Before pt", "true");
                    ProgressTask pt = new ProgressTask(this);
                    pt.execute(AppGlobal.ServerName + "socialLinkLogin/?email=" + user.getId() + "&type=tw");
                    Log.e("After pt", "true");

                } catch (Exception e) {
                    Log.e("Twitter Login Failed", e.getMessage() + " login failed");
                }
            }
        }
    }
}
