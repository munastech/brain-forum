package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import DataStructure.AppGlobal;
import DataStructure.User_data;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class Profile extends BaseActivity {
    EditText EtName, EtJobTitle, EtCompany, EtContactNumber, EtBiography, EtEmail, EtPassword, EtConfirmPassword;
    String Name, JobTitle, Company, ContactNumber, Biography, Email, id;
    ImageView profileImage;
    // number of images to select
    private static final int PICK_IMAGE = 1;
    private Bitmap bitmap;
    CheckBox show_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getAttendeeByID/" + AppGlobal.userId);
        SetViews();
        // SetValues();
    }

    public void SetViews() {
        EtName = (EditText) findViewById(R.id.name);
        EtJobTitle = (EditText) findViewById(R.id.job_title);
        EtCompany = (EditText) findViewById(R.id.company);
        EtContactNumber = (EditText) findViewById(R.id.contact_number);
        EtBiography = (EditText) findViewById(R.id.biography);
        EtEmail = (EditText) findViewById(R.id.email);
        EtPassword = (EditText) findViewById(R.id.password);
        EtConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        profileImage = (ImageView) findViewById(R.id.profile_image);
        show_contact = (CheckBox) findViewById(R.id.show_contact);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImageFromGallery();
            }
        });
    }

    public void SetValues() {
        if (AppGlobal.UserInfo != null) {
            for (int i = 0; i < AppGlobal.UserInfo.size(); i++) {
                final User_data obj = AppGlobal.UserInfo.get(0);
                id = obj.id;
                EtName.setText(obj.Name);
                EtJobTitle.setText(obj.JobTitle);
                EtCompany.setText(obj.Company);
                EtContactNumber.setText(obj.ContactNumber);
                if (obj.Biography != null && !obj.Biography.isEmpty())
                    EtBiography.setText(Html.fromHtml(obj.Biography));
                EtEmail.setText(obj.Email);
                if (obj.contact_info != null && obj.contact_info.equals("NO")) {
                    show_contact.setChecked(false);
                } else if (obj.contact_info != null && obj.contact_info.equals("YES")) {
                    show_contact.setChecked(true);
                }

                new AsyncTask<Void, Void, Void>() {
                    Drawable img = null;

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        profileImage.setImageDrawable(img);
                        try {
                            bitmap = ((BitmapDrawable) img).getBitmap();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            img = AppGlobal.grabImageFromUrl(AppGlobal.ServerImagePath + obj.Image);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();
            }

        }
    }

    /**
     * Opens dialog picker, so the user can select image from the gallery. The
     * result is returned in the method <code>onActivityResult()</code>
     */
    public void selectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            Log.d("Image path", picturePath);
            cursor.close();

            try {
                bitmap = AppGlobal.decodeFile(picturePath, profileImage);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void Update(View v) {
        GetValues();
        String contact_info = "NO";
        if (show_contact.isChecked()) {
            contact_info = "YES";
        }
        ImageUploadTask iut = new ImageUploadTask(this, bitmap, Name, JobTitle, Company, ContactNumber, Email, "", Biography, contact_info, "update", id);
        iut.execute();

    }

    public void GoBack(View v) {
        onBackPressed();
    }

    public void GetValues() {
        Name = EtName.getText().toString();
        JobTitle = EtJobTitle.getText().toString();
        Company = EtCompany.getText().toString();
        ContactNumber = EtContactNumber.getText().toString();
        Biography = EtBiography.getText().toString();
        Email = EtEmail.getText().toString();
    }

}
