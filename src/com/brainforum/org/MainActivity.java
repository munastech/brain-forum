package com.brainforum.org;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import org.apache.http.HttpEntity;

import DataStructure.AppGlobal;

public class MainActivity extends Activity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "1vw2oxdnA6qeYteNalpb8WWI1";
    private static final String TWITTER_SECRET = "tyncEHRsm5UlZd1cg4ziNjUeJXRORaGmPviYgo0Y7sOsOasHfp";
    // Asyntask
    AsyncTask<Void, Void, Void> mRegisterTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppGlobal.device_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        /*
		getActionBar().setTitle("");
        getActionBar().setIcon(null);
        */

        RegisterGCM();
        WaitAndGo();
    }

    private void RegisterGCM() {
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                AppGlobal.DISPLAY_MESSAGE_ACTION));
        GCMRegistrar.checkDevice(this);

        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);
        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);
        // Check if regid already presents
        if (regId.equals("")) {
            // Registration is not present, register now with GCM
            GCMRegistrar.register(this, AppGlobal.SENDER_ID);
        } else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // GCMRegistrar.register(this, AppGlobal.SENDER_ID);
                // Skips registration.
                //   Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server
                        // On server creates a new user
                        ServerUtilities.register(context, regId);
                        Toast.makeText(getApplicationContext(), "Device is registered newly", Toast.LENGTH_SHORT);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }
    }

    public void WaitAndGo() {
        CountDownTimer ctd = new CountDownTimer(5000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub
                SharedPreferences prefs = getSharedPreferences("my_prefs", Context.MODE_PRIVATE);
                AppGlobal.userId = prefs.getString("user_id", "");
                AppGlobal.userName = prefs.getString("name", "");
                Log.d("username/password", AppGlobal.userId + "/" + AppGlobal.userName);
                if (AppGlobal.userId == null || AppGlobal.userId.isEmpty()) {
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    MainActivity.this.finish();
                } else {
                    Intent i = new Intent(MainActivity.this, Home.class);
                    startActivity(i);
                    MainActivity.this.finish();
                }
            }
        }.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //broadcast reciver class to recive any new message
    private BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(AppGlobal.EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
            // Showing received message
            // lblMessage.append(newMessage + "\n");
            Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();
        }
    };

    @Override
    protected void onDestroy() {

        //
        //
        //	if (mHandleMessageReceiver != null) {
        //		unregisterReceiver(mHandleMessageReceiver);
        //		mHandleMessageReceiver = null;
        //	}
        //	//GCMRegistrar.onDestroy(getApplicationContext());
        //	super.onDestroy();
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(getApplicationContext());
        } catch (Exception e) {
			/*Log.e("UnRegister Receiver Error", "> " + e.getMessage());*/
        }
        super.onDestroy();
    }

    @Override
    public void finish() {
        super.finish();
        AppGlobal.ReleaseMemory();
    }
}
