package com.brainforum.org;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by Harris Rashid on 6/27/14.
 */
public class JSONParser {

    public JSONArray jsonArray;
    public JSONObject jsonObject;

    public JSONParser() {
        this.jsonArray = null;
        this.jsonObject = null;
    }

    public JSONArray getJSONArrayFromUrl(String[] args) {

        StringBuilder builder = getFromUrl(args);

        // Parse String to JSON object
        try {
            //Log.d("JSON Parser: ",builder.toString());
            if (builder != null)
                this.jsonArray = new JSONArray(builder.toString());
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }
        // return JSON Object
        return this.jsonArray;
    }

    public JSONObject getJSONObjectFromUrl(String[] args) {

        StringBuilder builder = getFromUrl(args);

        // Parse String to JSON object
        try {
            if ((builder != null) && (!builder.toString().equals("")))
                this.jsonObject = new JSONObject(builder.toString());
        } catch (JSONException e) {
            //e.printStackTrace();
            //Log.d("JSON Parser", "Error parsing data " + e.toString());
        } catch (Exception e) {
            //e.printStackTrace();
            //Log.d("Exception", "Error parsing data " + e.toString());
        }
        // return JSON Object
        return this.jsonObject;
    }

    public StringBuilder getFromUrl(String[] args) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(args[0]);

        try {
            Log.d("Arguments: ", args[0]);
           /* if ((args.length > 2) && (!args[1].equals("")) && (!args[2].equals(""))) {

                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                if (args[0].contains("checkUser.php")) {
                    nameValuePairs.add(new BasicNameValuePair("pin", args[1]));
                    nameValuePairs.add(new BasicNameValuePair("id", args[2]));
                    Log.d("Arguments: ", args[0] + args[1] + args[2]);
                } else {
                    nameValuePairs.add(new BasicNameValuePair("id", args[1]));
                }

                //Log.d("getFromUrl: ","Checking Client Id...");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                //Log.d("setEntity: ","setting Entity...");
            } else {

                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", args[1]));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            }*/

            HttpResponse response = client.execute(httpPost);
            Log.d("Response: ", response + "getting response...");
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            Log.d("statuscode: ", statusCode + "...");
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                //Log.d("Entity: ","getting entity...");
                InputStream content = entity.getContent();
                //Log.d("InputStream: ","I/S getting content...");
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                //Log.d("Reader: ","Read and Append to Sbuilder...");
                reader.close();
                content.close();
                //Log.d("Close: ","Reader & Input Stream close...");

            } else {
                Log.e("==>", "Failed to download file");
            }
        } catch (HttpHostConnectException e) {
            Log.d("HttpHostConnectException:", e.toString());
            //e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.d("ClientProtocolException:", e.toString());
            //e.printStackTrace();
        } catch (IOException e) {
            Log.d("IOException:", e.toString());
            //e.printStackTrace();
        } catch (Exception e) {
            Log.d("Exception:", e.toString());
            //e.printStackTrace();
        }
        return builder;
    }
}