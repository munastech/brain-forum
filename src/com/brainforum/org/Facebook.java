package com.brainforum.org;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Muhammad Nasir on 2/23/2015.
 */
public class Facebook extends BaseActivity {
    SharedPreferences prefs;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.facebook);
        prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
        url = prefs.getString("facebook", "https://www.facebook.com/TheBrainForum");
        SetViews();
    }

    public void SetViews() {
        WebView facebook = (WebView) findViewById(R.id.facebook);
        WebSettings settings = facebook.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        facebook.loadUrl(url);
        facebook.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                getWindow().setTitle(title); //Set Activity tile to page title.
            }
        });

        facebook.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
    }
}
