package com.brainforum.org;


import java.util.ArrayList;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

import DataStructure.AppGlobal;
import DataStructure.NotificationData;

public class GCMIntentService extends GCMBaseIntentService {


    private static final String TAG = "GCMIntentService";


    public GCMIntentService() {
        super(AppGlobal.SENDER_ID);
        //datasource = new CommentsDataSource(this);
        //datasource.open();
    }

    /**
     * Method called on device registered
     */
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        //displayMessage(context, "Your device registred with GCM");
        //		Log.d("NAME", SplashScreen.name);
        //		ServerUtilities.register(context, SplashScreen.name, SplashScreen.email, registrationId);
        SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
        prefs.edit().putString("gcm_id", registrationId).commit();
        ServerUtilities.register(context, registrationId);
    }

    /**
     * Method called on device un registred
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        // displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     */
    @Override
    protected void onMessage(Context context, Intent intent) {

        Log.i(TAG, "Received.................... message" + intent.getExtras().getString("notification_type"));

        if (intent.getExtras().getString("notification_type").equals("simple")) {
            AppGlobal.NotificationList = new ArrayList<NotificationData>();
            NotificationData obj = new NotificationData();
            obj.end_date = intent.getExtras().getString("end_date");
            obj.start_time = intent.getExtras().getString("start_time");
            obj.description = intent.getExtras().getString("description");
            obj.title = intent.getExtras().getString("title");
            obj.id = intent.getExtras().getString("id");
            AppGlobal.NotificationList.add(obj);

            generateNotification(context, intent.getExtras().getString("title"), "simple", "", "", "", "", "", "", "", "");
        } else if (intent.getExtras().getString("notification_type").equals("cron_job")) {
            String program_title = intent.getExtras().getString("program_title");
            String program_desc = intent.getExtras().getString("program_desc");
            String program_day = intent.getExtras().getString("program_day");
            String program_time = intent.getExtras().getString("program_time");
            String program_address = intent.getExtras().getString("program_address");
            String program_month = intent.getExtras().getString("program_month");
            String program_id = intent.getExtras().getString("program_id");
            String program_date = intent.getExtras().getString("program_date");
           generateNotification(context, intent.getExtras().getString("message"), "cron_job", program_id, program_time, program_month, program_day, program_date, program_title
                    , program_desc, program_address);

        }


    }

    /**
     * Method called on receiving a deleted message
     */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = "From Brain Forum: server deleted " + total + " pending messages!";
        // displayMessage(context, message);
        // notifies user
        generateNotification(context, message, "", "", "", "", "", "", "", "", "");
    }

    /**
     * Method called on Error
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        // displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        //displayMessage(context, getString(R.string.gcm_recoverable_error,
        // errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message, String type, String program_id, String program_time, String program_month,
                                             String program_day, String program_date, String program_title, String program_desc, String program_address) {
        int icon = R.drawable.app_icon;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);

        String title = context.getString(R.string.app_name);

        Intent notificationIntent = null;

        if (type.equalsIgnoreCase("simple")) {
            notificationIntent = new Intent(context, Notifications.class);

        }
        if (type.equalsIgnoreCase("cron_job")) {
            notificationIntent = new Intent(context, ProgramDesc.class);
            notificationIntent.putExtra("fromNotification", "true");
            SharedPreferences prefs = context.getSharedPreferences("my_prefs", context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("program_id", program_id);
            editor.putString("program_time", program_time);
            editor.putString("program_month", program_month);
            editor.putString("program_day", program_day);
            editor.putString("program_date", program_date);
            editor.putString("program_title", program_title);
            editor.putString("program_desc", program_desc);
            editor.putString("program_address", program_address);
            editor.commit();

            Log.d("Notification in generate notification month/day/date", program_month + "/" + program_day + "/" + program_date);
        }
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);

    }


}

