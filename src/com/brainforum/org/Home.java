package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.Session;

import DataStructure.AppGlobal;
import DataStructure.NotificationData;

/**
 * Created by Muhammad Nasir on 1/19/2015.
 */
public class Home extends BaseActivity {

    TextView mainText, user_name;
    LinearLayout menu_text_block, welcome_block;
    /* Shared preference keys */
    private static final String PREF_NAME = "sample_twitter_pref";
    private static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    private static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    private static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    private static final String PREF_USER_NAME = "twitter_user_name";
    private static SharedPreferences mSharedPreferences;
    public static boolean isUnreadNotifications = false;
    ImageView notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        SetViews();

        ManageActionBar();
      /*  mSharedPreferences=getSharedPreferences(PREF_NAME,MODE_PRIVATE);
        SharedPreferences.Editor e = mSharedPreferences.edit();
        e.putString(PREF_KEY_OAUTH_TOKEN, "");
        e.putString(PREF_KEY_OAUTH_SECRET, "");
        e.putBoolean(PREF_KEY_TWITTER_LOGIN, false);
        e.putString(PREF_USER_NAME, "");
        e.commit();*/
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "socialLink?user_id=" + AppGlobal.userId);
    }

    public void CheckNotification() {
        notification = (ImageView) findViewById(R.id.notifications);
        if (isUnreadNotifications) {
            notification.setImageResource(R.drawable.notification_red);
        }
    }

    private void ManageActionBar() {
        ActionBar mActionBar = getActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        ImageButton bUp = (ImageButton) mCustomView.findViewById(R.id.btn_up);
        if (!user_name.getText().toString().equalsIgnoreCase("guest")) {

            bUp.setVisibility(View.GONE);
        }
        bUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mActionBar.setCustomView(mCustomView);
        new ActionBar.LayoutParams(120, 220);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    private void SetViews() {
        mainText = (TextView) findViewById(R.id.mainText);
        user_name = (TextView) findViewById(R.id.user_name);
        menu_text_block = (LinearLayout) findViewById(R.id.menu_text_block);
        welcome_block = (LinearLayout) findViewById(R.id.welcome_block);
        if (AppGlobal.userName != null && !AppGlobal.userName.isEmpty()) {
            user_name.setText(AppGlobal.userName);
            menu_text_block.setVisibility(View.GONE);
            welcome_block.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
//        html = "<u><font color=\"" + colors[0] + "\" size=\"+20px\"> " + "No Cautions" + "</font></u><br/>";
        String text = "If you want a full access, please click here";
        Spanned sp = Html.fromHtml(text);
        mainText.setText(sp);
        mainText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Home.this, Register.class);
                startActivity(i);
                finish();
            }
        });
        if (AppGlobal.userId == null || AppGlobal.userId.equals("")) {
            ImageView sign_out = (ImageView) findViewById(R.id.sign_out);
            sign_out.setVisibility(View.GONE);
        } else {

        }
    }

    public void NextPage(View v) {
        Intent i;
        switch (v.getId()) {

            case R.id.about:
                i = new Intent(this, About.class);
                startActivity(i);
                break;
            case R.id.program:
                i = new Intent(this, ProgramList.class);
                startActivity(i);
                break;
            case R.id.speakers:
                i = new Intent(this, Speakers.class);
                startActivity(i);
                break;
            case R.id.attendees:
                if (AppGlobal.userId != null && !AppGlobal.userId.equals("")) {
                    i = new Intent(this, Attendees.class);
                    startActivity(i);
                } else {
                    MyDialog.showMessage("Alert", "You are Logged in as guest. Please register yourself to view this feature", Home.this);
                }
                break;
            case R.id.my_profiles:
                if (AppGlobal.userId != null && !AppGlobal.userId.equals("")) {
                    i = new Intent(this, Profile.class);
                    startActivity(i);
                } else {
                    MyDialog.showMessage("Alert", "You are Logged in as guest. Please register yourself to view this feature", Home.this);
                }
                break;
            case R.id.my_favorites:
                if (AppGlobal.userId != null && !AppGlobal.userId.equals("")) {
                    i = new Intent(this, FavoriteEvents.class);
                    startActivity(i);
                } else {
                    MyDialog.showMessage("Alert", "You are Logged in as guest. Please register yourself to view this feature", Home.this);
                }
                break;
            case R.id.gallery:
                i = new Intent(this, GalleryAlbums.class);
                startActivity(i);
                break;
            case R.id.sign_out:
                SharedPreferences.Editor prefs = getSharedPreferences("my_prefs", Context.MODE_PRIVATE).edit();
                prefs.putString("user_id", null);
                prefs.putString("name", null);
                prefs.commit();

                //log out twitter
                mSharedPreferences = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
                SharedPreferences.Editor e = mSharedPreferences.edit();
                e.putString(PREF_KEY_OAUTH_TOKEN, "");
                e.putString(PREF_KEY_OAUTH_SECRET, "");
                e.putBoolean(PREF_KEY_TWITTER_LOGIN, false);
                e.putString(PREF_USER_NAME, "");
                e.commit();

                //...logout facebook
                callFacebookLogout();

                i = new Intent(this, LoginActivity.class);
                startActivity(i);
                this.finish();
                break;
            case R.id.notifications:
                notification.setImageResource(R.drawable.menu);
                i = new Intent(this, Notifications.class);
                startActivity(i);
                break;
            case R.id.facebook:
                i = new Intent(this, Facebook.class);
                startActivity(i);
                break;
            case R.id.twitter:
                i = new Intent(this, TwitterApp.class);
                startActivity(i);
                break;
            default:
                break;

        }
    }

    /**
     * Logout From Facebook
     */
    public void callFacebookLogout() {
        Session session = Session.getActiveSession();
        if (session != null) {

            if (!session.isClosed()) {
                session.closeAndClearTokenInformation();
                //clear your preferences if saved
            }
        } else {

            session = new Session(this);
            Session.setActiveSession(session);

            session.closeAndClearTokenInformation();
            //clear your preferences if saved

        }

    }
}
