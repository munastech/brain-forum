package com.brainforum.org;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import DataStructure.AppGlobal;

/**
 * Created by Muhammad Nasir on 2/13/2015.
 */
class ImageUploadTask extends AsyncTask<Void, Void, String> {
    Activity activity;
    private ProgressDialog dialog;
    Bitmap bitmap;
    String Name, JobTitle, Company, ContactNumber, Email, Password, Biography, contact_info, type, id;

    ImageUploadTask(Activity ctx, Bitmap bitmap, String Name, String JobTitle,
                    String Company, String ContactNumber, String Email, String Password, String Biography
            , String contact_info, String type, String id) {
        activity = ctx;
        dialog = new ProgressDialog(activity);
        this.bitmap = bitmap;
        this.Name = Name;
        this.JobTitle = JobTitle;
        this.Company = Company;
        this.ContactNumber = ContactNumber;
        this.Email = Email;
        this.Password = Password;
        this.Biography = Biography;
        this.contact_info = contact_info;
        this.type = type;
        this.id = id;
    }

    @Override
    protected void onPreExecute() {
        dialog.setMessage("Please Wait...");
        dialog.show();
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            InputStream in = null;
            ByteArrayOutputStream stream = null;
            if (bitmap == null) {
                bitmap = BitmapFactory.decodeResource(activity.getResources(),
                        R.drawable.profile_pic);
            }
            stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // convert Bitmap to ByteArrayOutputStream
            in = new ByteArrayInputStream(stream.toByteArray()); // convert ByteArrayOutputStream to ByteArrayInputStream
            // Log.d("Image Stream", in.toString() + " bantoo");

            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = null;
            try {
                HttpPost httppost = null;

                MultipartEntity entity = new MultipartEntity();

                entity.addPart("user_image", "profile.jpg", in);
                entity.addPart("empty", "empty");
                entity.addPart("company", Company);
                entity.addPart("job_title", JobTitle);
                entity.addPart("contact_number", ContactNumber);
                entity.addPart("email", Email);
                entity.addPart("password", Password);
                entity.addPart("biography", Biography);
                entity.addPart("name", Name);
                entity.addPart("contact_info", contact_info);

                if (type.equals("add")) {
                    httppost = new HttpPost(
                            AppGlobal.ServerNameRegister + "registeration/"); // server
                } else {
                    httppost = new HttpPost(
                            AppGlobal.ServerNameRegister + "updateProfile/"); // server
                    entity.addPart("user_id", id);
                }
                httppost.setEntity(entity);

                Log.i("Request", "request" + httppost.getRequestLine());

                try {
                    response = httpclient.execute(httppost);
                } catch (ClientProtocolException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try {
                    if (response != null)
                        Log.i("Response", "response " + response.getStatusLine().toString());
                } finally {

                }
            } finally {

            }

            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            HttpEntity httpEntity = response.getEntity();
            String ResStr = EntityUtils.toString(httpEntity).replace("ï»¿", "");
            Log.i("Response String", "response " + ResStr);
            return ResStr;
        } catch (Exception e) {
            // something went wrong. connection with the server error
        }
        return null;
    }

    @Override
    protected void onPostExecute(String string) {
        if (dialog.isShowing())
            dialog.dismiss();
        Log.i("Response Str", "response " + string);
        if (string != null) {
            try {
                JSONObject jsonObj = new JSONObject(string);

                JSONObject regObj = null;

                regObj = jsonObj.getJSONObject("registration");

                if (regObj != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle("Alert");
                    builder.setMessage(regObj.getString("message"));
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    });
                    builder.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
