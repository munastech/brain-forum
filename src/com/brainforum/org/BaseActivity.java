package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import DataStructure.AppGlobal;

/**
 * Created by Muhammad Nasir on 1/30/2015.
 */
public class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();
        ActionBar mActionBar = getActionBar();
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);

        LayoutInflater mInflater = LayoutInflater.from(this);


        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        RelativeLayout searchBlock = (RelativeLayout) mCustomView.findViewById(R.id.searchBlock);
        searchBlock.setVisibility(View.VISIBLE);
        ImageView search = (ImageView) mCustomView.findViewById(R.id.search);
        ImageView bottomBar = (ImageView) mCustomView.findViewById(R.id.bottomBar);
        final EditText searchBar = (EditText) mCustomView.findViewById(R.id.searchBar);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchBar.setVisibility(View.VISIBLE);
            }
        });
        ImageButton bUp = (ImageButton) mCustomView.findViewById(R.id.btn_up);
        bUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        SharedPreferences prefs = getSharedPreferences("my_prefs", Context.MODE_PRIVATE);
        AppGlobal.userId = prefs.getString("user_id", "");
        AppGlobal.userName = prefs.getString("name", "");

        //
        if (this.getLocalClassName().equals("Home") || this.getLocalClassName().equals("Facebook")
                || this.getLocalClassName().equals("TwitterApp"))
            bottomBar.setVisibility(View.GONE);
    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }
    /**
     * Shows the soft keyboard
     */
 /*   public void showSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        view.requestFocus();
        inputMethodManager.showSoftInput(view, 0);
    }*/
}
