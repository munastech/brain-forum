package com.brainforum.org;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CalendarContract;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.zip.Inflater;

import DataStructure.AppGlobal;
import DataStructure.SpeakerData;

/**
 * Created by Muhammad Nasir on 1/23/2015.
 */
public class ProgramDesc extends BaseActivity {

    ImageView rate_this_session, favorite, register;
    TextView EtTime, EtDate, EtMonth, EtDay, EtAddress, EtProgram_name;
    WebView EtInformation;

    String program_id, program_time, program_month, program_day, program_date, program_title, program_desc, program_address;
    LinearLayout sessionRateBlock;
    ScrollView ContentScroll;
    public static boolean isFavorite = false;
    public static boolean isRated = false;
    public static String RateAlertMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.program_descreption);
        SetViews();
        SetValues();
    }

    private void SetValues() {

        Bundle intent = getIntent().getExtras();
       /* if (!intent.getString("fromNotification","").equals("true")) {
            program_id = intent.getString("program_id");
            program_time = intent.getString("program_time");
            program_month = intent.getString("program_month");
            program_day = intent.getString("program_day");
            program_date = intent.getString("program_date");
            program_title = intent.getString("program_title");
            program_desc = intent.getString("program_desc");
            program_address = intent.getString("program_address");
            Log.d("Event month/day/date", program_month + "/"+program_day+"/"+program_date);
        }else*/
        {
            SharedPreferences prefs = getSharedPreferences("my_prefs", MODE_PRIVATE);
            program_id = prefs.getString("program_id", "");
            program_time = prefs.getString("program_time", "");
            program_month = prefs.getString("program_month", "");
            program_day = prefs.getString("program_day", "");
            program_date = prefs.getString("program_date", "");
            program_title = prefs.getString("program_title", "");
            program_desc = prefs.getString("program_desc", "");
            program_address = prefs.getString("program_address", "");
        }
        SetValuesInViews();
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getProgarmSpeakers/?program_id=" + program_id + "&user_id=" + AppGlobal.userId);
    }

    public void AddFavoriteEvent(View v) {
        favorite.setImageResource(R.drawable.star_yellow);
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "favorites/?user_id=" + AppGlobal.userId + "&program_id=" + program_id, "favorite");

    }

    private void SetValuesInViews() {
        EtTime.setText(program_time);
        EtDay.setText(program_day);
        EtDate.setText(program_date);
        EtMonth.setText(program_month);
        EtProgram_name.setText(program_title);
        EtAddress.setText(program_address);
        if (program_desc != null && !program_desc.isEmpty())
            // EtInformation.loadData(program_desc,"text/html; charset=UTF-8", null);


            EtInformation.getSettings().setJavaScriptEnabled(true);
        if (AppGlobal.isTablet(this)) {
            EtInformation.getSettings().setTextSize(WebSettings.TextSize.LARGER);
        } else {
            EtInformation.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
        }
        //EtInformation.getSettings().setLoadWithOverviewMode(true);
        //EtInformation.getSettings().setUseWideViewPort(true);

        //EtInformation.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        EtInformation.loadDataWithBaseURL("", program_desc, "text/html", "UTF-8", "");
    }

    private void SetViews() {
        rate_this_session = (ImageView) findViewById(R.id.rate_this_session);
        favorite = (ImageView) findViewById(R.id.favorite);
        register = (ImageView) findViewById(R.id.register);
        EtInformation = (WebView) findViewById(R.id.information);
        EtTime = (TextView) findViewById(R.id.program_time);
        EtDate = (TextView) findViewById(R.id.program_date);
        EtMonth = (TextView) findViewById(R.id.program_month);
        EtDay = (TextView) findViewById(R.id.program_day);
        EtAddress = (TextView) findViewById(R.id.address);
        EtProgram_name = (TextView) findViewById(R.id.Program_name);
        sessionRateBlock = (LinearLayout) findViewById(R.id.sessionRateBlock);
        ContentScroll = (ScrollView) findViewById(R.id.ContentScroll);
        if (AppGlobal.userId == null || AppGlobal.userId.equals("")) {
            rate_this_session.setEnabled(false);
            favorite.setEnabled(false);
            register.setEnabled(false);
           /* sessionRateBlock.setVisibility(View.GONE);
            LinearLayout.LayoutParams scrollParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,0,1.0f);
            ContentScroll.setLayoutParams(scrollParams);*/
            sessionRateBlock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MyDialog.showMessage("Alert", "You are Logged in as guest. Please register yourself to view this feature", ProgramDesc.this);
                }
            });
        }


        rate_this_session.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isRated) {
                    Intent i = new Intent(ProgramDesc.this, SessionFeedback.class);
                    i.putExtra("session_id", program_id);
                    i.putExtra("session_title", program_title);
                    startActivity(i);
                } else {
                    MyDialog.showMessage("Alert", RateAlertMessage, ProgramDesc.this);
                }
            }
        });
    }

    public void AddEventToCalendar(View v) {
        Calendar cal = Calendar.getInstance();

        String[] timeArr = program_time.split(":");
        int startHour = Integer.parseInt(timeArr[0]);
        String[] midTimeArr = timeArr[1].split("\n");
        int startMinute = Integer.parseInt(midTimeArr[0]);
        int endHour = Integer.parseInt(midTimeArr[1]);
        int endMinute = Integer.parseInt(timeArr[2]);


        cal.set(Calendar.HOUR, startHour);
        cal.set(Calendar.MINUTE, startMinute);
        cal.set(Calendar.DATE, Integer.parseInt(program_date));

        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", cal.getTimeInMillis());
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, program_address);
        intent.putExtra(CalendarContract.Events.DESCRIPTION, Html.fromHtml(program_desc).toString());
        intent.putExtra("allDay", false);
        intent.putExtra(CalendarContract.Events.RDATE, program_date);
        cal.set(Calendar.DATE, Integer.parseInt(program_date));
        cal.set(Calendar.HOUR, endHour);
        cal.set(Calendar.MINUTE, endMinute);
        intent.putExtra("endTime", cal.getTimeInMillis());
        intent.putExtra(CalendarContract.Events.TITLE, program_title);
        startActivity(intent);
    }

    public void ListSpeakers() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.speakers);
        LayoutInflater inflater = getLayoutInflater();
        if (isFavorite) {
            favorite.setImageResource(R.drawable.star_yellow);
        }
        if (AppGlobal.EventSpeakers != null) {

            for (int i = 0; i < AppGlobal.EventSpeakers.size(); i++) {
                final SpeakerData obj = AppGlobal.EventSpeakers.get(i);
                LinearLayout llChild = (LinearLayout) inflater.inflate(R.layout.event_speaker_child, ll, false);
                final ImageView iv = (ImageView) llChild.findViewById(R.id.speaker_image);
                TextView tvname = (TextView) llChild.findViewById(R.id.speaker_name);
                TextView title = (TextView) llChild.findViewById(R.id.title);
                TextView tvbio = (TextView) llChild.findViewById(R.id.biography);
                tvname.setText(obj.name);
                tvbio.setText(obj.job_title);
                title.setText(obj.title+". ");
                final int k = i;
                llChild.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ProgramDesc.this, SpeakerDesc.class);
                        intent.putExtra("position", k);
                        intent.putExtra("fromEvents", true);
                        startActivity(intent);
                        ProgramDesc.this.finish();
                    }
                });

              /*  if (i == 1)
                    iv.setImageResource(R.drawable.img1);
                else
                    iv.setImageResource(R.drawable.img2);*/

                new AsyncTask<Void, Void, Void>() {
                    Drawable img = null;

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        iv.setImageDrawable(img);

                    }

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
                            img = AppGlobal.grabImageFromUrl(AppGlobal.ServerImagePath + obj.image_name);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute();

                ll.addView(llChild);
            }
        }
    }
}
