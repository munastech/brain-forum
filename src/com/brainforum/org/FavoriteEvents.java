package com.brainforum.org;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import DataStructure.AppGlobal;
import DataStructure.ProgramData;

import static DataStructure.AppGlobal.FavoritePrograms;
import static DataStructure.AppGlobal.FavoriteSessions;
import static DataStructure.AppGlobal.Sessions;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class FavoriteEvents extends BaseActivity {

    TextView heading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.programs);
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getFavoriteProgram/?user_id=" + AppGlobal.userId);
        //SetViews();
    }

    public void SetViews() {

        heading = (TextView) findViewById(R.id.heading);
        heading.setText("My Favorite Sessions");

        final TableLayout tbl = (TableLayout) findViewById(R.id.events);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (FavoritePrograms != null) {

            for (int i = 0; i < FavoritePrograms.size(); i++) {
                TableRow tr = (TableRow) inflater.inflate(R.layout.event_list_header, tbl, false);
                TextView date = (TextView) tr.findViewById(R.id.date);
                TextView month = (TextView) tr.findViewById(R.id.month);
                TextView day = (TextView) tr.findViewById(R.id.day);
                TextView day_count = (TextView) tr.findViewById(R.id.day_count);

                final ProgramData programObj = FavoritePrograms.get(i);
                date.setText(programObj.date);
                month.setText(programObj.month);
                day.setText(programObj.DAY);
                day_count.setText("Day" + programObj.daycount);
                final String ProgramDate = programObj.program_date;
                tbl.addView(tr);
                /*
                Log.d("Session array size", Sessions.size()+" size");
                */


                for (int j = 0; j < FavoriteSessions.size(); j++) {
                    final ProgramData SessionObj = FavoriteSessions.get(j);
                    if (SessionObj.program_date.equals(ProgramDate)) {
                        tr = (TableRow) inflater.inflate(R.layout.favorite_event_child, tbl, false);
                        TextView time = (TextView) tr.findViewById(R.id.event_time);
                        TextView eventName = (TextView) tr.findViewById(R.id.event_name);
                        TextView eventDesc = (TextView) tr.findViewById(R.id.event_desc);
                        ImageView delete = (ImageView) tr.findViewById(R.id.delete);
                        final TableRow finalTr = tr;
                        delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(FavoriteEvents.this);
                                builder.setTitle("Alert");
                                builder.setMessage("Are you sure you want to delete ?");
                                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        tbl.removeView(finalTr);

                                        ProgressTask pt = new ProgressTask(FavoriteEvents.this);
                                        pt.execute(AppGlobal.ServerName + "delFavorites/?program_id=" + SessionObj.program_id + "&user_id=" + AppGlobal.userId, "delete_favorites");
                                    }
                                });
                                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        return;
                                    }
                                });
                                builder.show();

                            }
                        });
                        time.setText(SessionObj.start_time + "\n" + SessionObj.end_time);
                        eventName.setText(SessionObj.program_name);
                        Spanned desc = Html.fromHtml(SessionObj.description);
                        Log.d("Description", desc.toString());
                        eventDesc.setText(desc);
                        final int k = j;
                        tr.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(FavoriteEvents.this, ProgramDesc.class);
                                i.putExtra("program_id", SessionObj.program_id);
                                i.putExtra("program_time", SessionObj.start_time + "\n" + SessionObj.end_time);
                                i.putExtra("program_month", programObj.month);
                                i.putExtra("program_day", programObj.DAY);
                                i.putExtra("program_date", programObj.date);
                                i.putExtra("program_title", SessionObj.program_name);
                                i.putExtra("program_desc", SessionObj.description);
                                startActivity(i);
                            }
                        });
                        tbl.addView(tr);

                    }
                }
            }
        }

    }


}
