package com.brainforum.org;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import Adapters.SpeakersAdapter;
import DataStructure.AppGlobal;
import DataStructure.SpeakerData;

/**
 * Created by Muhammad Nasir on 1/26/2015.
 */
public class Speakers extends BaseActivity {
    ListView speakerList;
    SpeakersAdapter adapter;
    EditText editsearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speakers);
        speakerList = (ListView) findViewById(R.id.speakerList);
/*if (AppGlobal.SpeakersList==null) {*/
        ProgressTask pt = new ProgressTask(this);
        pt.execute(AppGlobal.ServerName + "getSpeakers");
/*}else
{
    GenerateSpeakersList();
}*/

        //GenerateSpeakersList();

        Search();

    }

    public void Search() {
        ManageActionBar();
        // Locate the EditText in listview_main.xml
        editsearch = (EditText) findViewById(R.id.searchBar);

        // Capture Text in EditText
        editsearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                                          int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void ManageActionBar() {
        ActionBar mActionBar = getActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);

        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        editsearch = (EditText) mCustomView.findViewById(R.id.searchBar);
        ImageView search = (ImageView) mCustomView.findViewById(R.id.search);
        ImageView lineBlue = (ImageView) mCustomView.findViewById(R.id.lineBlue);
        lineBlue.setVisibility(View.VISIBLE);
        ImageButton bUp = (ImageButton) mCustomView.findViewById(R.id.btn_up);
        bUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editsearch.setVisibility(View.VISIBLE);
            }
        });
        search.setVisibility(View.VISIBLE);
        mActionBar.setCustomView(mCustomView);
        new ActionBar.LayoutParams(120, 220);
        mActionBar.setDisplayShowCustomEnabled(true);
    }

    public void GenerateSpeakersList() {
        if (AppGlobal.SpeakersList != null) {

            adapter = new SpeakersAdapter(this, R.layout.event_speaker_child, AppGlobal.SpeakersList);
            speakerList.setAdapter(adapter);
            speakerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(Speakers.this, SpeakerDesc.class);
                    intent.putExtra("position", i);
                    startActivity(intent);
                }
            });
        }
    }
}
