package com.brainforum.org;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import DataStructure.AppGlobal;

/**
 * Created by Muhammad Nasir on 3/12/2015.
 */
public class ForgotPassword extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password);
    }

    public void Submit(View v)
    {
        EditText email=(EditText)findViewById(R.id.email);
        if (!email.getText().toString().isEmpty()) {
            ProgressTask pt = new ProgressTask(this);
            pt.execute(AppGlobal.ServerName + "forgotPassword/?email=" + email.getText().toString() + "");
        }else
        {
            MyDialog.showMessage("Alert","Please enter your email",this);
        }
    }
}
