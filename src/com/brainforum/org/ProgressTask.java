package com.brainforum.org;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import DataStructure.AppGlobal;
import DataStructure.AttendeeData;
import DataStructure.FeedbackData;
import DataStructure.GalleryAlbumsData;
import DataStructure.GalleryData;
import DataStructure.NotificationData;
import DataStructure.ProgramData;
import DataStructure.SpeakerData;
import DataStructure.User_data;

/**
 * Created by Muhammad Nasir on 2/12/2015.
 */
public class ProgressTask extends AsyncTask<String, String, String> {

    Activity activity;
    String activityName;
    ProgressDialog pd;

    public ProgressTask(Activity ctx) {
        activity = ctx;
        activityName = ctx.getLocalClassName();
        pd = new ProgressDialog(ctx);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd.setTitle("Please Wait...");
        pd.show();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (pd.isShowing())
            pd.dismiss();
        if (!s.equals(">")) {
            if (activityName.equals("SessionFeedback")) {
                Intent intent = new Intent(activity, FeedbackResponse.class);
                intent.putExtra("message", s);
                activity.startActivity(intent);
                activity.finish();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Alert");
                builder.setMessage(s);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                });
                builder.show();
                //  Toast.makeText(activity, s, Toast.LENGTH_LONG).show();
            }
        } else if (activityName.equals("LoginActivity")) {
            Intent i = new Intent(activity, Home.class);
            activity.startActivity(i);
            activity.finish();
        } else if (activityName.equals("ProgramList")) {
            ProgramList list = (ProgramList) activity;
            list.SetViews();
        } else if (activityName.equals("ProgramDesc")) {
            ProgramDesc list = (ProgramDesc) activity;
            list.ListSpeakers();
        } else if (activityName.equals("GalleryAlbums")) {
            GalleryAlbums list = (GalleryAlbums) activity;
            list.SetViews();
        } else if (activityName.equals("Gallery")) {
            Gallery list = (Gallery) activity;
            list.GenerateList();
        } else if (activityName.equals("Speakers")) {
            Speakers list = (Speakers) activity;
            list.GenerateSpeakersList();
        } else if (activityName.equals("SpeakerDesc")) {
            SpeakerDesc list = (SpeakerDesc) activity;
            list.GenerateEventsList();
        } else if (activityName.equals("Attendees")) {
            Attendees list = (Attendees) activity;
            list.SetViews();
        } else if (activityName.equals("About")) {
            About obj = (About) activity;
            obj.SettingData();
        } else if (activityName.equals("FavoriteEvents")) {
            FavoriteEvents list = (FavoriteEvents) activity;
            list.SetViews();
        } else if (activityName.equals("Profile")) {
            Profile list = (Profile) activity;
            list.SetValues();
        } else if (activityName.equals("Notifications")) {
            Notifications list = (Notifications) activity;
            list.SetViews();
        } else if (activityName.equals("SessionFeedback")) {
            SessionFeedback list = (SessionFeedback) activity;
            list.SetViews();
        } else if (activityName.equals("Home")) {
            Home home = (Home) activity;
            home.CheckNotification();
        }
    }

    @Override
    protected String doInBackground(String... args) {
        if (args.length == 0) {
            //Wrong call
            return "No Url...!!!";
        } else if (!isOnline()) {
            //MyDialog.showMessage(this.activity.getTitle().toString(),"No Internet Connectivity",false,this.activity);
            return "No Internet Connectivity";
        }

        String message = ">";
        JSONParser Jparser = new JSONParser();
        try {
            if (activityName.equals("LoginActivity")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    SharedPreferences prefs = activity.getSharedPreferences("my_prefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();

                    if (args[0].contains("socialLinkLogin")) {
                        editor.putString("user_id", jsonObj.getString("user_id"));
                        editor.commit();
                    } else {
                        JSONObject login = jsonObj.getJSONObject("login");
                        if (login.getString("success").equals("0")) {
                            message = login.getString("message");
                        } else if (login.getString("success").equals("1")) {
                            editor.putString("user_id", login.getString("user_id"));
                            editor.putString("name", login.getString("name"));
                            editor.commit();
                            AppGlobal.userId = prefs.getString("user_id", "");
                            AppGlobal.userName = prefs.getString("name", "");
                        }
                    }
                }
            } else if (activityName.equals("ProgramList") || activityName.equals("FavoriteEvents")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    {
                        if (args.length < 2) {
                            Iterator<String> iterator = jsonObj.keys();
                            List<String> keysList = new ArrayList<String>();
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                keysList.add(key);
                            }
                            if (!activityName.equals("FavoriteEvents")) {
                                AppGlobal.Programs = new ArrayList<ProgramData>();
                                AppGlobal.Sessions = new ArrayList<ProgramData>();
                            } else {
                                AppGlobal.FavoritePrograms = new ArrayList<ProgramData>();
                                AppGlobal.FavoriteSessions = new ArrayList<ProgramData>();
                            }
                            Collections.reverse(keysList);
                            ListIterator<String> ListIterator = keysList.listIterator();
                            while (ListIterator.hasNext()) {

                                ProgramData programObj = new ProgramData();
                                String key = ListIterator.next();
                                JSONObject programBlock = jsonObj.getJSONObject(key);
                                //  Log.d("program data/date",programBlock.getString("program_date")+"/"+programBlock.getString("date"));
                                programObj.program_date = programBlock.getString("program_date");
                                programObj.date = programBlock.getString("date");
                                if (Integer.parseInt(programObj.date) < 10)
                                    programObj.date = "0" + programObj.date;

                                programObj.month = programBlock.getString("month");
                                programObj.DAY = programBlock.getString("DAY");
                                programObj.daycount = programBlock.getString("daycount");
                                if (Integer.parseInt(programObj.daycount) < 10)
                                    programObj.daycount = "0" + programObj.daycount;
                                if (!activityName.equals("FavoriteEvents")) {
                                    AppGlobal.Programs.add(programObj);
                                } else {
                                    AppGlobal.FavoritePrograms.add(programObj);
                                }
                                JSONArray programsArr = programBlock.getJSONArray("programs");
                                for (int i = 0; i < programsArr.length(); i++) {
                                    ProgramData SessionObj = new ProgramData();
                                    JSONObject sessionObj = programsArr.getJSONObject(i);
                                    //Log.d("program name",sessionObj.getString("name"));
                                    SessionObj.program_id = sessionObj.getString("program_id");
                                    SessionObj.program_name = sessionObj.getString("name");
                                    SessionObj.description = sessionObj.getString("description");
                                    SessionObj.latitude = sessionObj.getString("latitude");
                                    SessionObj.longitude = sessionObj.getString("longitude");
                                    SessionObj.address = sessionObj.getString("address");
                                    SessionObj.program_date = sessionObj.getString("program_date");
                                    try {
                                        SessionObj.start_time = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(sessionObj.getString("start_time")));
                                        SessionObj.end_time = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(sessionObj.getString("end_time")));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    SessionObj.image_name = sessionObj.getString("image_name");
                                    SessionObj.thumb_image_name = sessionObj.getString("thumb_image_name");
                                    SessionObj.create_date = sessionObj.getString("create_date");
                                    SessionObj.pub_status = sessionObj.getString("pub_status");
                                    if (!activityName.equals("FavoriteEvents")) {
                                        AppGlobal.Sessions.add(SessionObj);
                                    } else {
                                        AppGlobal.FavoriteSessions.add(SessionObj);
                                    }

                                }
                            }
                        } else {
                            JSONObject obj = jsonObj.getJSONObject("favorite");
                            if (obj.getString("success").equals("1")) {
                                message = obj.getString("message");
                            } else {
                                message = obj.getString("message");
                            }
                        }
                    }
                }
            } else if (activityName.equals("ProgramDesc")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    {
                        if (args.length < 2) {
                            AppGlobal.EventSpeakers = new ArrayList<SpeakerData>();
                            JSONObject SpeakerObj = jsonObj.getJSONObject("programs");

                            //setting favorite and rating buttons
                            if (SpeakerObj.getString("isFavorite").equals("true")) {
                                ProgramDesc.isFavorite = true;
                            } else {
                                ProgramDesc.isFavorite = false;
                            }
                            if (SpeakerObj.getString("isRated").equals("true")) {
                                ProgramDesc.isRated = true;
                            } else {
                                ProgramDesc.isRated = false;
                            }
                            if (SpeakerObj.getString("alert_message") != null)
                                ProgramDesc.RateAlertMessage = SpeakerObj.getString("alert_message");
                            ///
                            JSONArray speakerArr = SpeakerObj.getJSONArray("speakers");
                            for (int i = 0; i < speakerArr.length(); i++) {
                                JSONObject speakerObj = speakerArr.getJSONObject(i);
                                SpeakerData obj = new SpeakerData();
                                obj.id = speakerObj.getString("speaker_id");
                                obj.title = speakerObj.getString("title");
                                obj.name = speakerObj.getString("name");
                                obj.job_title = speakerObj.getString("job_title");
                                obj.company = speakerObj.getString("company");
                                obj.image_name = speakerObj.getString("image_name");
                                obj.contact = speakerObj.getString("email");
                                JSONArray bioArr = speakerObj.getJSONArray("biography");
                                String bio = "";
                                for (int k = 0; k < bioArr.length(); k++) {
                                    JSONObject bioObj = bioArr.getJSONObject(k);
                                    bio += bioObj.getString("biography") + ",,";
                                }
                                Log.d("Biography Str", bio);
                                obj.biography = bio;
                                AppGlobal.EventSpeakers.add(obj);
                            }
                        } else {
                            JSONObject Obj = jsonObj.getJSONObject("favorite");
                            if (Obj.getString("success").equals("1")) {
                                message = Obj.getString("message");
                            } else {
                                message = Obj.getString("message");
                            }
                        }
                    }
                }
            } else if (activityName.equals("GalleryAlbums")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    AppGlobal.GalleryAlbumList = new ArrayList<GalleryAlbumsData>();
                    JSONArray AlbumArr = jsonObj.getJSONArray("album");
                    for (int i = 0; i < AlbumArr.length(); i++) {
                        JSONObject AlbumObj = AlbumArr.getJSONObject(i);
                        GalleryAlbumsData obj = new GalleryAlbumsData();
                        obj.id = AlbumObj.getString("id");
                        obj.image = AlbumObj.getString("image_name");
                        obj.description = AlbumObj.getString("description");
                        obj.title = AlbumObj.getString("name");
                        AppGlobal.GalleryAlbumList.add(obj);
                    }
                }

            } else if (activityName.equals("Gallery")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    AppGlobal.galleryList = new ArrayList<GalleryData>();
                    JSONArray galleryArr = jsonObj.getJSONArray("gallery");
                    for (int i = 0; i < galleryArr.length(); i++) {
                        JSONObject galleryObj = galleryArr.getJSONObject(i);
                        GalleryData obj = new GalleryData();
                        obj.id = galleryObj.getString("id");
                        obj.title = galleryObj.getString("image_title");
                        obj.date = galleryObj.getString("datetime");
                        obj.image_name = galleryObj.getString("image_name");
                        AppGlobal.galleryList.add(obj);
                    }
                }
            } else if (activityName.equals("Speakers")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    AppGlobal.SpeakersList = new ArrayList<SpeakerData>();
                    JSONArray SpeakerArr = jsonObj.getJSONArray("speakers");
                    for (int i = 0; i < SpeakerArr.length(); i++) {
                        JSONObject SpeakersObj = SpeakerArr.getJSONObject(i);
                        SpeakerData SD = new SpeakerData();
                        SD.id = SpeakersObj.getString("speaker_id");
                        SD.title = SpeakersObj.getString("title");
                        SD.name = SpeakersObj.getString("name");
                        SD.speakerFullName=SD.title+". "+SD.name;
                        SD.job_title = SpeakersObj.getString("job_title");
                        SD.company = SpeakersObj.getString("company");
                        SD.contact = SpeakersObj.getString("email");
                        SD.image_name = SpeakersObj.getString("image_name");
                        JSONArray bioArr = SpeakersObj.getJSONArray("biography");
                        String bio = "";
                        for (int k = 0; k < bioArr.length(); k++) {
                            JSONObject bioObj = bioArr.getJSONObject(k);
                            bio += bioObj.getString("biography") + ",,";
                        }
                        Log.d("Biography Str", bio);
                        SD.biography = bio;
                        AppGlobal.SpeakersList.add(SD);

                    }
                }
            } else if (activityName.equals("SpeakerDesc")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    AppGlobal.SpeakerEvents = new ArrayList<ProgramData>();
                    JSONArray SpeakerArr = jsonObj.getJSONArray("programs");
                    for (int i = 0; i < SpeakerArr.length(); i++) {
                        JSONObject EventObj = SpeakerArr.getJSONObject(i);
                        ProgramData obj = new ProgramData();
                        obj.program_name = EventObj.getString("name");
                        obj.program_id = EventObj.getString("program_id");
                        obj.description = EventObj.getString("description");
                        obj.date = EventObj.getString("date");
                        obj.month = EventObj.getString("month");
                        obj.DAY = EventObj.getString("DAY");
                        try {
                            obj.start_time = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(EventObj.getString("start_time")));
                            obj.end_time = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(EventObj.getString("end_time")));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        obj.address = EventObj.getString("address");
                        AppGlobal.SpeakerEvents.add(obj);
                    }
                }
            } else if (activityName.equals("Attendees")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    AppGlobal.AttendeesList = new ArrayList<AttendeeData>();
                    JSONArray AttendeeArr = jsonObj.getJSONArray("attendees");
                    for (int i = 0; i < AttendeeArr.length(); i++) {
                        JSONObject AttendeeObj = AttendeeArr.getJSONObject(i);
                        AttendeeData obj = new AttendeeData();
                        obj.id = AttendeeObj.getString("attendee_id");
                        obj.name = AttendeeObj.getString("name");
                        obj.biography = AttendeeObj.getString("biography");
                        obj.company_name = AttendeeObj.getString("company");
                        obj.email = AttendeeObj.getString("email");
                        obj.image = AttendeeObj.getString("image_name");
                        obj.job_title = AttendeeObj.getString("job_title");
                        obj.phone_number = AttendeeObj.getString("contact_number");
                        obj.contact_info = AttendeeObj.getString("contact_info");
                        AppGlobal.AttendeesList.add(obj);
                    }
                }
            } else if (activityName.equals("About")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    AppGlobal.About = null;
                    AppGlobal.About = jsonObj.getString("about_bf") + "@@" + jsonObj.getString("mis_vis");
                    JSONArray sliderArr = jsonObj.getJSONArray("slider");
                    AppGlobal.AboutImages = new ArrayList<String>();
                    for (int i = 0; i < sliderArr.length(); i++) {
                        JSONObject sliderObj = sliderArr.getJSONObject(i);
                        AppGlobal.AboutImages.add(sliderObj.getString("image_name"));
                    }
                }
            } else if (activityName.equals("Profile")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {

                    JSONArray AttendeeArr = jsonObj.getJSONArray("attendee");
                    for (int i = 0; i < AttendeeArr.length(); i++) {
                        AppGlobal.UserInfo = new ArrayList<User_data>();
                        JSONObject AttendeeObj = AttendeeArr.getJSONObject(i);
                        User_data obj = new User_data();
                        obj.id = AttendeeObj.getString("attendee_id");
                        obj.Name = AttendeeObj.getString("name");
                        obj.JobTitle = AttendeeObj.getString("job_title");
                        obj.Company = AttendeeObj.getString("company");
                        obj.ContactNumber = AttendeeObj.getString("contact_number");
                        obj.Biography = AttendeeObj.getString("biography");
                        obj.Email = AttendeeObj.getString("email");
                        obj.Image = AttendeeObj.getString("image_name");
                        obj.contact_info = AttendeeObj.getString("contact_info");
                        AppGlobal.UserInfo.add(obj);
                    }
                }
            } else if (activityName.equals("Notifications")) {
                if (args[0].contains("del_notification")) {
                    JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                    JSONObject notiObj = jsonObj.getJSONObject("notification");
                    message = notiObj.getString("message");

                } else {
                    JSONArray jsonArr = Jparser.getJSONArrayFromUrl(args);
                    if (jsonArr != null) {
                        AppGlobal.NotificationList = new ArrayList<NotificationData>();
                        for (int i = 0; i < jsonArr.length(); i++) {
                            JSONObject jsonObj = jsonArr.getJSONObject(i);
                            NotificationData obj = new NotificationData();
                            obj.id = jsonObj.getString("id");
                            obj.title = jsonObj.getString("title");
                            obj.description = jsonObj.getString("description");
                            try {
                                obj.start_time = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(jsonObj.getString("start_time")));
                                obj.end_date = new SimpleDateFormat("HH:mm").format(new SimpleDateFormat("HH:mm:ss").parse(jsonObj.getString("end_date")));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            AppGlobal.NotificationList.add(obj);
                        }
                    }
                }

            } else if (activityName.equals("SessionFeedback")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    if (args.length < 2) {

                        AppGlobal.FeedbackList = new ArrayList<FeedbackData>();
                        JSONArray AttendeeArr = jsonObj.getJSONArray("question");
                        for (int i = 0; i < AttendeeArr.length(); i++) {
                            JSONObject FeedbackObj = AttendeeArr.getJSONObject(i);
                            FeedbackData obj = new FeedbackData();
                            if (i < 10)
                                obj.number = "0" + String.valueOf(i + 1);
                            else
                                obj.number = String.valueOf(i + 1);
                            obj.id = FeedbackObj.getString("question_id");
                            obj.question = FeedbackObj.getString("question");
                            obj.rating = Float.parseFloat(FeedbackObj.getString("question_id"));
                            AppGlobal.FeedbackList.add(obj);
                        }
                    } else {
                        JSONObject obj = jsonObj.getJSONObject("feedback");
                        if (obj.getString("success").equals("1")) {
                            message = obj.getString("message");
                        } else {
                            message = obj.getString("message");
                        }
                    }
                }

            } else if (activityName.equals("Home")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj != null) {
                    SharedPreferences prefs = activity.getSharedPreferences("my_prefs", activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("twitter_official", jsonObj.getString("soc_tw"));
                    editor.putString("twitter_unofficial", jsonObj.getString("soc_tw_un"));
                    editor.putString("facebook", jsonObj.getString("soc_fb"));
                    editor.commit();
                    if (jsonObj.getString("status").equals("1"))
                        Home.isUnreadNotifications = true;
                    else
                        Home.isUnreadNotifications = false;
                }
            }else if (activityName.equals("ForgorPassword")) {
                JSONObject jsonObj = Jparser.getJSONObjectFromUrl(args);
                if (jsonObj!=null) {
                    JSONObject ForgotObj = jsonObj.getJSONObject("forgot");
                    message = ForgotObj.getString("message");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) this.activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
