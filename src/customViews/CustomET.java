package customViews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Muhammad Nasir on 1/19/2015.
 */
public class CustomET extends EditText{
    public CustomET(Context context) {
        super(context);
        init();
    }
    public CustomET(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomET(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "roboto-Light.ttf");
            setTypeface(tf);
            //setTextColor(Color.parseColor("#777777"));
        }
    }
}
