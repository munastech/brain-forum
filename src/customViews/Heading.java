package customViews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Muhammad Nasir on 1/19/2015.
 */
public class Heading extends TextView{
    public Heading(Context context) {
        super(context);
        init();
    }

    public Heading(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Heading(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "roboto-Thin.ttf");
            setTypeface(tf);
        }
    }

}
