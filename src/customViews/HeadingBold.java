package customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Muhammad Nasir on 1/21/2015.
 */
public class HeadingBold extends TextView {
    public HeadingBold(Context context) {
        super(context);
    }

    public HeadingBold(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeadingBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    private void init()
    {
        Typeface tf=Typeface.createFromAsset(getContext().getAssets(), "Roboto-Bold.ttf");
        setTypeface(tf);
    }
}
