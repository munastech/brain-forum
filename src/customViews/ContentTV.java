package customViews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Muhammad Nasir on 1/19/2015.
 */
public class ContentTV extends TextView{
    public ContentTV(Context context) {
        super(context);
        init();
    }

    public ContentTV(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ContentTV(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "roboto-Light.ttf");
            setTypeface(tf);
        }
    }

}
