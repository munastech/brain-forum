package DataStructure;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class FeedbackData {
    public String id;
    public String number;
    public String question;
    public float rating;

    public FeedbackData() {

    }
}
