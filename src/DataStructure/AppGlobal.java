package DataStructure;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Muhammad Nasir on 1/27/2015.
 */
public class AppGlobal {
    public static String userId;
    public static String userName;
    public static String ServerNameRegister = "http://brainfourm.edesignbeta.net/index.php/androidservice/";
    public static String ServerName = "http://brainfourm.edesignbeta.net/index.php/services/";
    public static String ServerImagePath = "http://brainfourm.edesignbeta.net/";
    public static List<FeedbackData> FeedbackList;
    public static List<AttendeeData> AttendeesList;
    public static List<SpeakerData> SpeakersList;
    public static List<GalleryData> galleryList;
    public static List<GalleryAlbumsData> GalleryAlbumList;
    public static List<ProgramData> Programs;
    public static List<ProgramData> Sessions;
    public static List<SpeakerData> EventSpeakers;
    public static List<ProgramData> SpeakerEvents;
    public static String About;
    public static List<String> AboutImages;
    public static List<ProgramData> FavoritePrograms;
    public static List<ProgramData> FavoriteSessions;
    public static List<User_data> UserInfo;
    public static List<NotificationData> NotificationList;

    // Google project id
    public static final String SENDER_ID = "395277859336";//"48349391098"; //
    public static final String DISPLAY_MESSAGE_ACTION =
            "com.brainforum.org.DISPLAY_MESSAGE";

    public static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p/>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    public static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public static String device_id = "";

    public static int getResourceId(Context context, String name, String resourceType) {
        return context.getResources().getIdentifier(name, resourceType, context.getPackageName());
    }

    public static Drawable grabImageFromUrl(String url) throws IOException {
        Bitmap x;

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);
        return new BitmapDrawable(x);
    }

    public static Bitmap decodeFile(String filePath, ImageView imageView) {
        Log.d("Image path", filePath);
        File f = new File(filePath);
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(f.getAbsolutePath(), o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), o2);

        imageView.setImageBitmap(bitmap);
        return bitmap;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void ReleaseMemory() {
        userId = null;
        userName = null;
        FeedbackList = null;
        AttendeesList = null;
        SpeakersList = null;
        galleryList = null;
        GalleryAlbumList = null;
        Programs = null;
        Sessions = null;
        EventSpeakers = null;
        SpeakerEvents = null;
        About = null;
        AboutImages = null;
        FavoritePrograms = null;
        FavoriteSessions = null;
        UserInfo = null;
        NotificationList = null;
    }
}
