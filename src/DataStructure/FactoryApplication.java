package DataStructure;

/**
 * Created by Muhammad Nasir on 2/6/2015.
 */

import android.app.Application;
import android.content.Context;

import com.brainforum.org.BuildConfig;

public class FactoryApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
       /* if ( BuildConfig.USE_CRASHLYTICS ) {
           // Crashlytics.
        }
        FactoryApplication.context = getApplicationContext();
        TwitterAuthConfig authConfig =
                         new TwitterAuthConfig("consumerKey",
                                                     "consumerSecret");
        Fabric.with(this, new Twitter(authConfig),
                new Crashlytics());
    }*/

    }

    public static Context getAppContext() {
        return FactoryApplication.context;
    }
}
