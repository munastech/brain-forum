package DataStructure;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

public class ImageDownloader {

    String iconsStoragePath;//= FactoryApplication.getAppContext().getExternalFilesDir(null)+ "/FactoryApp/Cache/";
    //String  iconsStoragePath= FactoryApplication.getAppContext().getExternalFilesDir(null).toString();

    //String iconsStoragePath = ContextWrapper.getFilesDir() +"/FactoryApp/Cache/";
    public ImageDownloader() {
        try {
            iconsStoragePath = Environment.getExternalStorageDirectory().toString() + "/BrainForum/Cache/";
        } catch (Exception ex) {
            iconsStoragePath = "";
        }

    }

    public void downloadImage(String imgName) {
        imgName = imgName.replaceAll(" ", "%20");
        String url = AppGlobal.ServerImagePath + imgName;


        //get path to external storage (SD card)
        //String iconsStoragePath = FactoryApplication.getAppContext().getExternalFilesDir(null)+ "/FactoryApp/Cache/";
        File sdIconStorageDir = new File(iconsStoragePath);

        //create storage directories, if they don't exist
        if (!sdIconStorageDir.exists()) {

            sdIconStorageDir.mkdirs();
        }

        //		//Setting up cache directory to store the image
        //File cacheDir=new File(mContext.getCacheDir(),"factory");
        //
        //		// Check if cache folder exists, otherwise create folder.
        //if(!cacheDir.exists())cacheDir.mkdirs();

        // Setting up file to write the image to.
        File f = new File(sdIconStorageDir, imgName);

        // Open InputStream to download the image.
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new URL(url).openStream();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Set up OutputStream to write data into image file.

        try {
            os = new FileOutputStream(f);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CopyStream(is, os);
    }


    /**
     * Copy all data from InputStream and write using OutputStream
     *
     * @param is InputStream
     * @param os OutputStream
     */
    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public Bitmap getImage(String imageName) {

        File f = new File(iconsStoragePath + imageName);
        if (f.exists()) {

            Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());

            return bmp;
        }
        return null;
    }

    public void deleteImage(String imageName) {

        File f = new File(iconsStoragePath + imageName);
        if (f.exists()) {

            f.delete();
        }

    }
}
