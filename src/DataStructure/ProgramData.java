package DataStructure;

/**
 * Created by Muhammad Nasir on 2/13/2015.
 */
public class ProgramData {
    public String program_date, date, month, DAY, daycount, program_id, program_name, description, latitude, longitude, address,
            start_time, end_time, image_name, thumb_image_name, create_date, pub_status;

    public ProgramData() {
    }
}
